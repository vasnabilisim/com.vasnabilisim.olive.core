package com.vasnabilisim.olive.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.ErrorException;
import com.vasnabilisim.core.FatalException;
import com.vasnabilisim.core.Logger;

/**
 * User related functionalities.
 * @author Menderes Fatih GUVEN
 */
public final class User {

	/**
	 * Private constructor.
	 */
	private User() {
	}
	
	private static File userFolder = null;
	public static final File getUserFolder() {
		if(userFolder != null)
			return userFolder;
		
		String userHomeName = System.getProperty("user.home");
		File userHomeFolder = new File(userHomeName);
		if(!userHomeFolder.exists())
			new FatalException(String.format("Unable to find folder %s.", userHomeName));
		
		userFolder = new File(userHomeFolder, ".olive"); //$NON-NLS-1$
		if(!userFolder.exists()) {
			Logger.info(String.format("Unable to find folder %s.", userFolder.getAbsolutePath()));
			userFolder.mkdir();
		}
		if(!userFolder.exists())
			new FatalException(String.format("Unable to create folder %s.", userFolder.getAbsolutePath()));
		
		return userFolder;
	}
	
	public static File getUserFile(String fileName, boolean create) {
		File userFile = new File(getUserFolder(), fileName);
		if(create) {
			if(!userFile.exists()) {
				try {
					userFile.createNewFile();
				} catch (IOException e) {
					new FatalException(String.format("Unable to create file %s.", userFile.getAbsolutePath()), e);
				}
			}
		}
		return userFile;
	}

	public static String getUserFileContentAsString(String fileName) throws BaseException {
		File userFile = getUserFile(fileName, false);
		if(userFile == null)
			return null;
		FileInputStream fis = null;
		InputStreamReader reader = null;
		try {
			fis = new FileInputStream(userFile);
			reader = new InputStreamReader(fis, "UTF-8"); //$NON-NLS-1$
			char[] buffer = new char[128];
			int length = -1;
			StringBuilder builder = new StringBuilder(128);
			while( (length = reader.read(buffer, 0, buffer.length)) > 0 )
				builder.append(buffer, 0, length);
			return builder.toString();
		} catch (IOException e) {
			throw new ErrorException(String.format("Unable to read file %s.", userFile.getAbsolutePath()), e);
		} finally {
			try { if (reader != null) reader.close(); } catch (IOException e) { }
			try { if (fis != null) fis.close(); } catch (IOException e) { }
		}
	}
}
