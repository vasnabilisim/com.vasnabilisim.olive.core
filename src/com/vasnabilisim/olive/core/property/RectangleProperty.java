package com.vasnabilisim.olive.core.property;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.vasnabilisim.olive.core.geom.Rectangle;

/**
 * Rectangle Property.
 * 
 * @author Menderes Fatih GUVEN
 */
public class RectangleProperty extends AbstractProperty<Rectangle> {
	private static final long serialVersionUID = -43695806387588386L;

	public RectangleProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public RectangleProperty(String group, String alias, String name, String description, Rectangle value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public RectangleProperty(RectangleProperty source) {
		super(source);
	}

	@Override
	public RectangleProperty clone() {
		return new RectangleProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	public String getId() {
		return "Rectangle";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	public Class<Rectangle> getValueType() {
		return Rectangle.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	public String getStringValue() {
		return value == null ? null : value.toString();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	public void setStringValue(String stringValue) {
		value = Rectangle.valueOf(stringValue);
	}

	@Override
	public void writeXmlElement(Document document, Element element) {
		if(value != null) {
			createXmlElement(document, element, "X", value.x);
			createXmlElement(document, element, "Y", value.y);
			createXmlElement(document, element, "Width", value.w);
			createXmlElement(document, element, "Height", value.h);
		}
	}
	
	@Override
	public void readXmlElement(Document document, Element element) throws PropertyException {
		value = new Rectangle();
		value.x = readXmlElement(document, element, "X", value.x);
		value.y = readXmlElement(document, element, "Y", value.y);
		value.w = readXmlElement(document, element, "Width", value.w);
		value.h = readXmlElement(document, element, "Height", value.h);
	}
}
