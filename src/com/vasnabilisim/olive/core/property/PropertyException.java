package com.vasnabilisim.olive.core.property;

/**
 * Exception class of the solution.
 * @author Menderes Fatih GUVEN
 */
public class PropertyException extends Exception {
	private static final long serialVersionUID = 4526170357125435075L;

	/**
	 * Default constructor.
	 */
	public PropertyException() {
		super();
	}
	
	/**
	 * Message constructor.
	 * @param message
	 */
	public PropertyException(String message) {
		super(message);
	}
	
	/**
	 * Cause constructor.
	 * @param e
	 */
	public PropertyException(Throwable e) {
		super(e);
	}
	
	/**
	 * Compound constructor.
	 * @param message
	 * @param e
	 */
	public PropertyException(String message, Throwable e) {
		super(message, e);
	}
}
