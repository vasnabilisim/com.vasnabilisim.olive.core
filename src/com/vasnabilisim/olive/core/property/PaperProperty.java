package com.vasnabilisim.olive.core.property;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.geom.Insets;
import com.vasnabilisim.olive.core.paper.Paper;
import com.vasnabilisim.olive.core.paper.PaperOrientation;
import com.vasnabilisim.olive.core.paper.PaperUnit;


/**
 * Page Property.
 * 
 * @author Menderes Fatih GUVEN
 */
public class PaperProperty extends AbstractProperty<Paper> {
	private static final long serialVersionUID = -4338081736870097642L;

	public PaperProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public PaperProperty(String group, String alias, String name, String description, Paper value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public PaperProperty(PaperProperty source) {
		super(source);
	}

	@Override
	public PaperProperty clone() {
		return new PaperProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	public String getId() {
		return "Page";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	@Override
	public Class<Paper> getValueType() {
		return Paper.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	public String getStringValue() {
		return value == null ? null : value.toString();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	public void setStringValue(String stringValue) {
		value = Paper.valueOf(stringValue);
		if(value == null)
			value = Paper.Default.clone();
	}

	@Override
	public void writeXmlElement(Document document, Element element) {
		if(value != null) {
			createXmlElement(document, element, "Name", value.getName());
			Element childElement = createXmlElement(document, element, "Size");
			createXmlElement(document, childElement, "Width", value.getSize().w);
			createXmlElement(document, childElement, "Height", value.getSize().h);
			childElement = createXmlElement(document, element, "Margin");
			createXmlElement(document, childElement, "Left", value.getMargin().l);
			createXmlElement(document, childElement, "Right", value.getMargin().r);
			createXmlElement(document, childElement, "Top", value.getMargin().t);
			createXmlElement(document, childElement, "Bottom", value.getMargin().b);
			createXmlElement(document, element, "Orientation", value.getOrientation());
			createXmlElement(document, element, "Unit", value.getUnit());
		}
	}
	
	@Override
	public void readXmlElement(Document document, Element element) throws PropertyException {
		String name = readXmlElement(document, element, "Name", "Custom");
		Dimension size = Paper.Default.getSize().clone();
		Element childElement = readXmlElement(document, element, "Size");
		size.w = readXmlElement(document, childElement, "Width", size.w);
		size.h = readXmlElement(document, childElement, "Height", size.h);
		Insets margin = Paper.Default.getMargin().clone();
		childElement = readXmlElement(document, element, "Margin");
		margin.l = readXmlElement(document, childElement, "Left", margin.l);
		margin.r = readXmlElement(document, childElement, "Right", margin.r);
		margin.t = readXmlElement(document, childElement, "Top", margin.t);
		margin.b = readXmlElement(document, childElement, "Bottom", margin.b);
		PaperOrientation orientation = readXmlElement(document, element, "Orientation", PaperOrientation.class, Paper.Default.getOrientation());
		PaperUnit unit = readXmlElement(document, element, "Unit", PaperUnit.class, Paper.Default.getUnit());
		value = new Paper(name, size, margin, orientation, unit);
	}
}
