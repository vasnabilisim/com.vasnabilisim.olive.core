package com.vasnabilisim.olive.core.property;


/**
 * Boolean Property.
 * @author Menderes Fatih GUVEN
 */
public class BooleanProperty extends AbstractProperty<Boolean> {
	private static final long serialVersionUID = -8225941176838586672L;

	public BooleanProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public BooleanProperty(String group, String alias, String name, String description, Boolean value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public BooleanProperty(BooleanProperty source) {
		super(source);
	}

	@Override
	public BooleanProperty clone() {
		return new BooleanProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	public String getId() {
		return "Boolean";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	public Class<Boolean> getValueType() {
		return Boolean.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	public String getStringValue() {
		return value == null ? null : value.toString();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	public void setStringValue(String stringValue) {
		value = Boolean.valueOf(stringValue);
	}
}
