package com.vasnabilisim.olive.core.property;

import com.vasnabilisim.olive.core.geom.VerticalAlignment;


/**
 * Vertical Alignment Property.
 * @see com.vasnabilisim.olive.core.geom.VerticalAlignment
 * @author Menderes Fatih GUVEN
 */
public class VerticalAlignmentProperty extends AbstractEnumProperty<VerticalAlignment> {
	private static final long serialVersionUID = 7232500172390448601L;

	public VerticalAlignmentProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public VerticalAlignmentProperty(String group, String alias, String name, String description, VerticalAlignment value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public VerticalAlignmentProperty(VerticalAlignmentProperty source) {
		super(source);
	}

	@Override
	public VerticalAlignmentProperty clone() {
		return new VerticalAlignmentProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	@Override
	public String getId() {
		return "VerticalAlignment";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	@Override
	public Class<VerticalAlignment> getValueType() {
		return VerticalAlignment.class;
	}
}
