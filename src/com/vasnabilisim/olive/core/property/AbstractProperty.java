package com.vasnabilisim.olive.core.property;

import java.util.Collections;
import java.util.EnumSet;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.vasnabilisim.reflect.ReflectionException;
import com.vasnabilisim.reflect.ReflectionUtil;
import com.vasnabilisim.util.Cloneable;
import com.vasnabilisim.util.EqualsBuilder;
import com.vasnabilisim.xml.XmlParser;

/**
 * Abstract base class of all properties.
 * 
 * @author Menderes Fatih GUVEN
 */
public abstract class AbstractProperty<V> implements Property<V> {
	private static final long serialVersionUID = 970213713451021982L;
	
	/**
	 * Group of the property.
	 */
	protected String group = null;

	/**
	 * Alias of the property.
	 */
	protected String alias = null;

	/**
	 * Name of the property.
	 */
	protected String name = null;

	/**
	 * Description of the property.
	 */
	protected String description = null;

	/**
	 * Value of the property.
	 */
	protected V value = null;

	/**
	 * Default constructor.
	 */
	protected AbstractProperty() {
	}

	/**
	 * Value constructor.
	 * 
	 * @param group Group of the property.
	 * @param alias Alias of the property.
	 * @param key Name of the property.
	 * @param description Description of the property.
	 * @param value Value of the property.
	 */
	protected AbstractProperty(String group, String alias, String name, String description, V value) {
		this.group = group;
		this.alias = alias;
		this.name = name;
		this.description = description;
		this.value = value;
	}

	/**
	 * Copy constructor;
	 * 
	 * @param source
	 */
	protected AbstractProperty(AbstractProperty<V> source) {
		this.group = source.group;
		this.alias = source.alias;
		this.name = source.name;
		this.description = source.description;
		if(Cloneable.class.isAssignableFrom(getValueType()))
			this.value = getValueType().cast( ((Cloneable<?>)source.value).cloneObject() );
		else
			this.value = source.value;
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public abstract AbstractProperty<V> clone();

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getGroup()
	 */
	@Override
	public String getGroup() {
		return group;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setGroup(java.lang.String)
	 */
	@Override
	public void setGroup(String group) {
		this.group = group;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getAlias()
	 */
	@Override
	public String getAlias() {
		return alias;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setAlias(java.lang.String)
	 */
	@Override
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getDescription()
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValue()
	 */
	@Override
	public V getValue() {
		return value;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setValue(java.io.Serializable)
	 */
	@Override
	public V setValue(V value) {
		this.value = value;
		return this.value;
	}
	
	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setValue(com.vasnabilisim.olive.core.property.Property)
	 */
	@Override
	public AbstractProperty<V> setValue(Property<V> property) {
		setValue(property == null ? null : property.getValue());
		return this;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setObjectValue(java.lang.Object)
	 */
	@Override
	public V setObjectValue(Object value) {
		try {
			setValue(getValueType().cast(value));
		} catch (ClassCastException e) {
		}
		return this.value;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj == this)
			return true;
		if (!(obj instanceof AbstractProperty))
			return false;
		AbstractProperty<?> other = (AbstractProperty<?>) obj;
		return new EqualsBuilder().append(this.group, other.group).append(this.alias, other.alias).append(this.name, other.name).toBoolean();
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(100);
		sb.append("<");
		sb.append(getGroup());
		sb.append(", ");
		sb.append(getAlias());
		sb.append(", ");
		sb.append(getName());
		sb.append(", ");
		sb.append(getStringValue());
		sb.append(">");
		return sb.toString();
	}

	/**
	 * Sets given elements text content as stringValue
	 * @see com.vasnabilisim.olive.core.property.Property#writeXmlElement(org.w3c.dom.Document, org.w3c.dom.Element)
	 */
	@Override
	public void writeXmlElement(Document document, Element element) {
		element.setTextContent(getStringValue());
	}

	/**
	 * Creates an element with given name and returns it.
	 * @param document
	 * @param element
	 * @param name
	 * @return
	 */
	protected Element createXmlElement(Document document, Element element, String name) {
		Element childElement = document.createElement(name);
		element.appendChild(childElement);
		return childElement;
	}

	/**
	 * Creates an element with given name and appends given value as text content. 
	 * Returns the element.
	 * @param document
	 * @param element
	 * @param name
	 * @param value
	 * @return
	 */
	protected Element createXmlElement(Document document, Element element, String name, String value) {
		Element childElement = createXmlElement(document, element, name);
		childElement.setTextContent(value);
		return childElement;
	}
	
	/**
	 * Creates an element with given name and appends given value as text content. 
	 * Returns the element.
	 * @param document
	 * @param element
	 * @param name
	 * @param value
	 * @return
	 */
	protected Element createXmlElement(Document document, Element element, String name, int value) {
		return createXmlElement(document, element, name, String.valueOf(value));
	}
	
	/**
	 * Creates an element with given name and appends given value as text content. 
	 * Returns the element.
	 * @param document
	 * @param element
	 * @param name
	 * @param value
	 * @return
	 */
	protected Element createXmlElement(Document document, Element element, String name, float value) {
		return createXmlElement(document, element, name, String.valueOf(value));
	}
	
	/**
	 * Creates an element with given name and appends given value as text content. 
	 * Returns the element.
	 * @param document
	 * @param element
	 * @param name
	 * @param value
	 * @return
	 */
	protected <E extends Enum<E>> Element createXmlElement(Document document, Element element, String name, E value) {
		return createXmlElement(document, element, name, value == null ? null : value.name());
	}

	/**
	 * Sets given elements text content as string value.
	 * @see com.vasnabilisim.olive.core.property.Property#readXmlElement(org.w3c.dom.Document, org.w3c.dom.Element)
	 */
	@Override
	public void readXmlElement(Document document, Element element) throws PropertyException {
		setStringValue(XmlParser.getElementText(element));
	}

	/**
	 * Returns the first child element under given parentElement having given name. 
	 * Returns null if no such element exists.
	 * @param document
	 * @param parentElement
	 * @param name
	 * @return
	 */
	protected Element readXmlElement(Document document, Element parentElement, String name) {
		if(document == null || parentElement == null)
			return null;
		return XmlParser.findChildElement(parentElement, name);
	}

	/**
	 * Returns all child elements under given parentElement having given name.
	 * Returns empty list if no such element exists.
	 * @param document
	 * @param parentElement
	 * @param name
	 * @return
	 */
	protected List<Element> readXmlElements(Document document, Element parentElement, String name) {
		if(document == null || parentElement == null)
			return Collections.emptyList();
		return XmlParser.findChildElements(parentElement, name);
	}

	/**
	 * Returns the text content of the first child element under given parentElement having given name. 
	 * Returns given defaultValue if no such element exists.
	 * @param document
	 * @param parentElement
	 * @param name
	 * @param defaultValue
	 * @return
	 */
	protected String readXmlElement(Document document, Element parentElement, String name, String defaultValue) {
		Element childElement = readXmlElement(document, parentElement, name);
		if(childElement == null)
			return defaultValue;
		return XmlParser.getElementText(childElement, defaultValue);
	}

	/**
	 * Returns the integer value of the text content of the first child element under given parentElement having given name. 
	 * Returns given defaultValue if no such element exists.
	 * @param document
	 * @param parentElement
	 * @param name
	 * @param defaultValue
	 * @return
	 */
	protected int readXmlElement(Document document, Element parentElement, String name, int defaultValue) {
		Element childElement = XmlParser.findChildElement(parentElement, name);
		if(childElement == null)
			return defaultValue;
		return XmlParser.getElementTextAsInt(childElement, defaultValue);
	}

	/**
	 * Returns the float value of the text content of the first child element under given parentElement having given name. 
	 * Returns given defaultValue if no such element exists.
	 * @param document
	 * @param parentElement
	 * @param name
	 * @param defaultValue
	 * @return
	 */
	protected float readXmlElement(Document document, Element parentElement, String name, float defaultValue) {
		Element childElement = XmlParser.findChildElement(parentElement, name);
		if(childElement == null)
			return defaultValue;
		return XmlParser.getElementTextAsFloat(childElement, defaultValue);
	}

	/**
	 * Returns the value of the text content of the first child element under given parentElement having given name. 
	 * Returns null if no such element exists.
	 * @param document
	 * @param parentElement
	 * @param name
	 * @param valueType
	 * @return
	 */
	protected <X> X readXmlElement(Document document, Element parentElement, String name, Class<X> valueType) {
		Element childElement = XmlParser.findChildElement(parentElement, name);
		String stringValue = null;
		if(childElement != null)
			stringValue = XmlParser.getElementText(childElement);
		try {
			return ReflectionUtil.invoke(valueType, valueType, "valueOf", String.class, null, stringValue);
		} catch (ReflectionException e) {
			return null;
		}
	}

	/**
	 * Returns the value of the text content of the first child element under given parentElement having given name. 
	 * Returns given defaultValue if no such element exists.
	 * @param document
	 * @param parentElement
	 * @param name
	 * @param valueType
	 * @param defaultValue
	 * @return
	 */
	protected <X> X readXmlElement(Document document, Element parentElement, String name, Class<X> valueType, X defaultValue) {
		Element childElement = XmlParser.findChildElement(parentElement, name);
		if(childElement == null)
			return defaultValue;
		String stringValue = XmlParser.getElementText(childElement);
		try {
			return ReflectionUtil.invoke(valueType, valueType, "valueOf", String.class, null, stringValue);
		} catch (ReflectionException e) {
			return defaultValue;
		}
	}

	/**
	 * Returns the enum value parsed from the text content of the first child element under given parentElement having given name. 
	 * Returns given defaultValue if no such element exists.
	 * @param document
	 * @param parentElement
	 * @param name
	 * @param enumClass
	 * @param defaultValue
	 * @return
	 */
	protected <E extends Enum<E>> E readXmlElement(Document document, Element parentElement, String name, Class<E> enumClass, E defaultValue) {
		Element childElement = XmlParser.findChildElement(parentElement, name);
		if(childElement == null)
			return defaultValue;
		return XmlParser.getElementTextAsEnum(childElement, enumClass, defaultValue);
	}
	
	/**
	 * Returns the enum values parsed from the text content of all child elements under given parentElement having given name. 
	 * Returns empty set if no such element exists.
	 * @param document
	 * @param parentElement
	 * @param name
	 * @param enumClass
	 * @return
	 */
	protected <E extends Enum<E>> EnumSet<E> readXmlElements(Document document, Element parentElement, String name, Class<E> enumClass) {
		EnumSet<E> enumSet = EnumSet.noneOf(enumClass);
		if(document == null || parentElement == null)
			return enumSet;
		List<Element> childElements = readXmlElements(document, parentElement, name);
		for(Element childElement : childElements) {
			E e = readXmlElement(document, childElement, name, enumClass, null);
			if(e != null)
				enumSet.add(e);
		}
		return enumSet;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#writeXmlAttribute(org.w3c.dom.Document, org.w3c.dom.Element)
	 */
	@Override
	public void writeXmlAttribute(Document document, Element element) throws PropertyException {
		element.setAttribute(this.name, getStringValue());
	}

	protected void writeXmlAttribute(Document document, Element element, String attributeName, String attributeValue) throws PropertyException {
		element.setAttribute(this.name, getStringValue());
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#readXmlAttribute(org.w3c.dom.Document, org.w3c.dom.Element)
	 */
	@Override
	public void readXmlAttribute(Document document, Element element) throws PropertyException {
		setStringValue(XmlParser.getAtributeValue(element, this.name));
	}
}
