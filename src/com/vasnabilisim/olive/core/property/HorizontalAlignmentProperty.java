package com.vasnabilisim.olive.core.property;

import com.vasnabilisim.olive.core.geom.HorizontalAlignment;


/**
 * Horizontal Alignment Property.
 * @see com.vasnabilisim.olive.core.geom.HorizontalAlignment
 * @author Menderes Fatih GUVEN
 */
public class HorizontalAlignmentProperty extends AbstractEnumProperty<HorizontalAlignment> {
	private static final long serialVersionUID = -5598326078513875130L;

	public HorizontalAlignmentProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public HorizontalAlignmentProperty(String group, String alias, String name, String description, HorizontalAlignment value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public HorizontalAlignmentProperty(HorizontalAlignmentProperty source) {
		super(source);
	}

	@Override
	public HorizontalAlignmentProperty clone() {
		return new HorizontalAlignmentProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	@Override
	public String getId() {
		return "HorizontalAlignment";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	@Override
	public Class<HorizontalAlignment> getValueType() {
		return HorizontalAlignment.class;
	}
}
