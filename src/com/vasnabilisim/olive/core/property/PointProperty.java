package com.vasnabilisim.olive.core.property;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.vasnabilisim.olive.core.geom.Point;

/**
 * Point Property.
 * 
 * @author Menderes Fatih GUVEN
 */
public class PointProperty extends AbstractProperty<Point> {
	private static final long serialVersionUID = 530323395747262327L;

	public PointProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public PointProperty(String group, String alias, String name, String description, Point value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public PointProperty(PointProperty source) {
		super(source);
	}

	@Override
	public PointProperty clone() {
		return new PointProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	public String getId() {
		return "Point";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	public Class<Point> getValueType() {
		return Point.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	public String getStringValue() {
		return value == null ? null : value.toString();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	public void setStringValue(String stringValue) {
		value = Point.valueOf(stringValue);
	}

	@Override
	public void writeXmlElement(Document document, Element element) {
		if(value != null) {
			createXmlElement(document, element, "X", value.x);
			createXmlElement(document, element, "Y", value.y);
		}
	}
	
	@Override
	public void readXmlElement(Document document, Element element) throws PropertyException {
		value = Point.Zero.clone();
		value.x = readXmlElement(document, element, "X", value.x);
		value.y = readXmlElement(document, element, "Y", value.y);
	}
}
