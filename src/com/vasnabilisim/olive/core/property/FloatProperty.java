package com.vasnabilisim.olive.core.property;


/**
 * Float Property.
 * @author Menderes Fatih GUVEN
 */
public class FloatProperty extends AbstractBoundableProperty<Float> {
	private static final long serialVersionUID = -3299642017948480373L;

	public FloatProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public FloatProperty(String group, String alias, String name, String description, Float value) {
		super(group, alias, name, description, value, -Float.MAX_VALUE, Float.MAX_VALUE);
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 * @param minimumValue
	 * @param maximumValue
	 */
	public FloatProperty(String group, String alias, String name, String description, Float value, Float minimumValue, Float maximumValue) {
		super(group, alias, name, description, value, minimumValue, maximumValue);
	}

	/**
	 * @param source
	 */
	public FloatProperty(FloatProperty source) {
		super(source);
	}

	@Override
	public FloatProperty clone() {
		return new FloatProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	@Override
	public String getId() {
		return "Float";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	@Override
	public Class<Float> getValueType() {
		return Float.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	@Override
	public String getStringValue() {
		if(value == null)
			return null;
		return value.toString();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	@Override
	public void setStringValue(String stringValue) {
		try {
			value = Float.valueOf(stringValue);
		}
		catch (NumberFormatException e) {
			value = new Float(0.0f);
		}
	}
}
