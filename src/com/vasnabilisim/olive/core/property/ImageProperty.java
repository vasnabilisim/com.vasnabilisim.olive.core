package com.vasnabilisim.olive.core.property;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;

import com.vasnabilisim.core.Logger;

/**
 * String Property.
 * @author Menderes Fatih GUVEN
 */
public class ImageProperty extends AbstractProperty<BufferedImage> {
	private static final long serialVersionUID = -7755944115838080039L;

	public ImageProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public ImageProperty(String group, String alias, String name, String description, BufferedImage value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public ImageProperty(ImageProperty source) {
		super(source);
	}

	@Override
	public ImageProperty clone() {
		return new ImageProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	@Override
	public String getId() {
		return "Image";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	@Override
	public Class<BufferedImage> getValueType() {
		return BufferedImage.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	@Override
	public String getStringValue() {
		if(value == null)
			return null;
		ByteArrayOutputStream out = new ByteArrayOutputStream(value.getWidth() * value.getHeight() * value.getColorModel().getPixelSize() / 8);
		try {
			ImageIO.write(value, "png", out);
		} catch (IOException e) {
			Logger.error(e);
			return null;
		}
		return Base64.getUrlEncoder().encodeToString(out.toByteArray());
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	@Override
	public void setStringValue(String stringValue) {
		if(stringValue == null) {
			value = null;
			return;
		}
		byte[] bytes = Base64.getUrlDecoder().decode(stringValue);
		try {
			value = ImageIO.read(new ByteArrayInputStream(bytes));
		} catch (IOException e) {
			Logger.error(e);
			value = null;
		}
	}
}
