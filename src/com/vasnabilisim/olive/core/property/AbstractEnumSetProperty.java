package com.vasnabilisim.olive.core.property;

import java.util.EnumSet;
import java.util.StringTokenizer;


/**
 * Abstract Enum Set Property .
 * @author Menderes Fatih GUVEN
 */
@SuppressWarnings("rawtypes")
public abstract class AbstractEnumSetProperty<E extends Enum<E>> extends AbstractProperty<EnumSet> {
	private static final long serialVersionUID = -1508571892750238220L;

	/**
	 * Default constructor.
	 */
	public AbstractEnumSetProperty() {
		super();
		value = EnumSet.noneOf(getEnumType());
	}

	/**
	 * Group constructor
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public AbstractEnumSetProperty(String group, String alias, String name, String description, EnumSet<E> value) {
		super(group, alias, name, description, value);
		value = value == null ? EnumSet.noneOf(getEnumType()) : value;
	}

	/**
	 * @param source
	 */
	public AbstractEnumSetProperty(AbstractEnumSetProperty<E> source) {
		super(source);
	}
	
	@Override
	public abstract AbstractEnumSetProperty<E> clone();
	
	@Override
	public Class<EnumSet> getValueType() {
		return EnumSet.class;
	}
	
	public abstract Class<E> getEnumType();

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	@Override
	public String getStringValue() {
		StringBuilder builder = new StringBuilder(value.size() * 10);
		for(Object e : value) {
			if(builder.length() > 0)
				builder.append(',');
			builder.append(getEnumType().cast(e).name());
		}
		return builder.toString();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void setStringValue(String stringValue) {
		try {
			StringTokenizer tokenizer = new StringTokenizer(stringValue, ",", false);
			value.clear();
			while(tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken().trim();
				value.add(Enum.valueOf(getEnumType(), token));
			}
		} catch (Exception e) {
			value.clear();
		}
	}
}
