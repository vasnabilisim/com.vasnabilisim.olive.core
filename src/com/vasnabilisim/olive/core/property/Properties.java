package com.vasnabilisim.olive.core.property;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import java.util.TreeMap;

import com.vasnabilisim.util.Cloneable;

/**
 * Property collection.
 * @author Menderes Fatih GUVEN
 */
public class Properties implements Cloneable<Properties>, java.lang.Cloneable, Serializable {
	private static final long serialVersionUID = 7347277238636503207L;

	TreeMap<String, PropertyGroup> propertyGroupMap = new TreeMap<>();
	
	/**
	 * Default constructor.
	 */
	public Properties() {
		super();
	}
	
	/**
	 * Copy constructor.
	 * @param source
	 */
	public Properties(Properties source) {
		for(PropertyGroup propertyGroup : source.propertyGroupMap.values())
			this.propertyGroupMap.put(propertyGroup.getName(), propertyGroup.cloneObject());
	}

	@Override
	public Properties cloneObject() {
		return new Properties(this);
	}
	
	@Override
	protected Object clone() {
		return cloneObject();
	}
	
	public Property<?> get(String groupName, String propertyName) {
		Objects.requireNonNull(groupName);
		Objects.requireNonNull(propertyName);
		PropertyGroup propertyGroup = propertyGroupMap.get(groupName);
		if(propertyGroup == null)
			return null;
		return propertyGroup.get(propertyName);
	}
	
	public Property<?> get(String propertyName) {
		Objects.requireNonNull(propertyName);
		Property<?> property;
		for(PropertyGroup propertyGroup : propertyGroupMap.values()) {
			property = propertyGroup.get(propertyName);
			if(property != null)
				return property;
		}
		return null;
	}
	
	public Object getValue(String groupName, String propertyName) {
		Property<?> property = get(groupName, propertyName);
		return property == null ? null : property.getValue();
	}
	
	public Object getValue(String propertyName) {
		Property<?> property = get(propertyName);
		return property == null ? null : property.getValue();
	}
	
	public <V> V getValue(String groupName, String propertyName, Class<V> valueType) {
		Objects.requireNonNull(valueType);
		Property<?> property = get(groupName, propertyName);
		return property == null ? null : valueType.cast(property.getValue());
	}
	
	public <V> V getValue(String groupName, String propertyName, Class<V> valueType, V defaultValue) {
		Objects.requireNonNull(valueType);
		Property<?> property = get(groupName, propertyName);
		return property == null || property.getValue() == null ? defaultValue : valueType.cast(property.getValue());
	}
	
	public <V> V getValue(String propertyName, Class<V> valueType, V defaultValue) {
		Objects.requireNonNull(valueType);
		Property<?> property = get(propertyName);
		return property == null || property.getValue() == null ? defaultValue : valueType.cast(property.getValue());
	}
	
	@SuppressWarnings("unchecked")
	public <V> V getValue(String groupName, String propertyName, V defaultValue) {
		Property<?> property = get(groupName, propertyName);
		return property == null || property.getValue() == null ? defaultValue : (V)property.getValue();
	}
	
	@SuppressWarnings("unchecked")
	public <V> V getValue(String propertyName, V defaultValue) {
		Property<?> property = get(propertyName);
		return property == null || property.getValue() == null ? defaultValue : (V)property.getValue();
	}
	
	public <V> V getValue(String propertyName, Class<V> valueType) {
		Objects.requireNonNull(valueType);
		Property<?> property = get(propertyName);
		return property == null ? null : valueType.cast(property.getValue());
	}
	
	public float getValue(String groupName, String propertyName, float defaultValue) {
		Property<?> property = get(groupName, propertyName);
		return property == null || property.getValue() == null ? defaultValue : (Float)property.getValue();
	}
	
	public float getValue(String propertyName, float defaultValue) {
		Property<?> property = get(propertyName);
		return property == null || property.getValue() == null ? defaultValue : (Float)property.getValue();
	}
	
	public int getValue(String groupName, String propertyName, int defaultValue) {
		Property<?> property = get(groupName, propertyName);
		return property == null || property.getValue() == null ? defaultValue : (Integer)property.getValue();
	}
	
	public int getValue(String propertyName, int defaultValue) {
		Property<?> property = get(propertyName);
		return property == null || property.getValue() == null ? defaultValue : (Integer)property.getValue();
	}
	
	@SuppressWarnings("unchecked")
	public <E extends Enum<E>> E getValue(String groupName, String propertyName, E defaultValue) {
		Property<?> property = get(groupName, propertyName);
		return property == null || property.getValue() == null ? null : (E)property.getValue();
	}
	
	@SuppressWarnings("unchecked")
	public <E extends Enum<E>> E getValue(String propertyName, E defaultValue) {
		Property<?> property = get(propertyName);
		return property == null || property.getValue() == null ? null : (E)property.getValue();
	}
	
	public void setValue(String groupName, String propertyName, Object value) {
		Property<?> property = get(groupName, propertyName);
		if(property == null)
			return;
		property.setObjectValue(value);
	}
	
	public void setValue(String propertyName, Object value) {
		Property<?> property = get(propertyName);
		if(property == null)
			return;
		property.setObjectValue(value);
	}
	
	public Property<?> add(Property<?> property) {
		Objects.requireNonNull(property);
		PropertyGroup propertyGroup = propertyGroupMap.get(property.getGroup());
		if(propertyGroup == null) {
			propertyGroup = new PropertyGroup(property.getGroup());
			propertyGroupMap.put(propertyGroup.getName(), propertyGroup);
		}
		return propertyGroup.add(property);
	}
	
	public void addAll(Properties source) {
		Objects.requireNonNull(source);
		for(PropertyGroup sourcePropertyGroup : source.propertyGroupMap.values()) {
			PropertyGroup thisPropertyGroup = this.propertyGroupMap.get(sourcePropertyGroup.getName());
			if(thisPropertyGroup == null)
				this.propertyGroupMap.put(sourcePropertyGroup.getName(), sourcePropertyGroup);
			else
				thisPropertyGroup.addAll(sourcePropertyGroup);
		}
	}
	
	public Property<?> remove(String groupName, String propertyName) {
		Objects.requireNonNull(groupName);
		Objects.requireNonNull(propertyName);
		PropertyGroup propertyGroup = propertyGroupMap.get(groupName);
		if(propertyGroup == null)
			return null;
		return propertyGroup.remove(propertyName);
	}
	
	public Property<?> remove(Property<?> property) {
		Objects.requireNonNull(property);
		PropertyGroup propertyGroup = propertyGroupMap.get(property.getGroup());
		if(propertyGroup == null)
			return null;
		return propertyGroup.remove(property.getName());
	}

	public boolean contains(String groupName, String propertyName) {
		Objects.requireNonNull(groupName);
		Objects.requireNonNull(propertyName);
		PropertyGroup propertyGroup = propertyGroupMap.get(groupName);
		if(propertyGroup == null)
			return false;
		return propertyGroup.contains(propertyName);
	}
	
	public boolean contains(Property<?> property) {
		Objects.requireNonNull(property);
		PropertyGroup propertyGroup = propertyGroupMap.get(property.getGroup());
		if(propertyGroup == null)
			return false;
		return propertyGroup.contains(property.getName());
	}
	
	public PropertyGroup getGroup(String groupName) {
		Objects.requireNonNull(groupName);
		return propertyGroupMap.get(groupName);
	}

	public PropertyGroup addGroup(PropertyGroup propertyGroup) {
		Objects.requireNonNull(propertyGroup);
		return propertyGroupMap.put(propertyGroup.getName(), propertyGroup);
	}
	
	public Collection<PropertyGroup> getGroups() {
		return propertyGroupMap.values();
	}
}
