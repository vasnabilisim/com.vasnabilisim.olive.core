package com.vasnabilisim.olive.core.property;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.vasnabilisim.olive.core.paint.Border;
import com.vasnabilisim.olive.core.paint.Color;

/**
 * Border Property.
 * 
 * @author Menderes Fatih GUVEN
 */
public class BorderProperty extends AbstractProperty<Border> {
	private static final long serialVersionUID = -4244141320537146673L;

	public BorderProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public BorderProperty(String group, String alias, String name, String description, Border value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public BorderProperty(BorderProperty source) {
		super(source);
	}

	@Override
	public BorderProperty clone() {
		return new BorderProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	public String getId() {
		return "Border";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	public Class<Border> getValueType() {
		return Border.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	public String getStringValue() {
		return value == null ? null : value.toString();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	public void setStringValue(String stringValue) {
		value = Border.valueOf(stringValue);
	}

	@Override
	public void writeXmlElement(Document document, Element element) {
		if(value != null) {
			createXmlElement(document, element, "LeftThickness", value.thickness.l);
			createXmlElement(document, element, "LeftColor", (value.lColor == null ? Color.Transparent : value.lColor).toString());
			createXmlElement(document, element, "TopThickness", value.thickness.t);
			createXmlElement(document, element, "TopColor", (value.tColor == null ? Color.Transparent : value.tColor).toString());
			createXmlElement(document, element, "RightThickness", value.thickness.r);
			createXmlElement(document, element, "RightColor", (value.rColor == null ? Color.Transparent : value.rColor).toString());
			createXmlElement(document, element, "BottomThickness", value.thickness.b);
			createXmlElement(document, element, "BottomColor", (value.bColor == null ? Color.Transparent : value.bColor).toString());
		}
	}
	
	@Override
	public void readXmlElement(Document document, Element element) throws PropertyException {
		value = Border.Empty.clone();
		value.thickness.l = readXmlElement(document, element, "LeftThickness", value.thickness.l);
		value.lColor = readXmlElement(document, element, "LeftColor", Color.class, Color.Transparent);
		value.thickness.t = readXmlElement(document, element, "TopThickness", value.thickness.t);
		value.tColor = readXmlElement(document, element, "TopColor", Color.class, Color.Transparent);
		value.thickness.r = readXmlElement(document, element, "RightThickness", value.thickness.r);
		value.rColor = readXmlElement(document, element, "RightColor", Color.class, Color.Transparent);
		value.thickness.b = readXmlElement(document, element, "BottomThickness", value.thickness.b);
		value.bColor = readXmlElement(document, element, "BottomColor", Color.class, Color.Transparent);
	}
}
