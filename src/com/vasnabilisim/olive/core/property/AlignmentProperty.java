package com.vasnabilisim.olive.core.property;

import com.vasnabilisim.olive.core.geom.Alignment;


/**
 * Alignment Property.
 * @see com.vasnabilisim.olive.core.geom.Alignment
 * @author Menderes Fatih GUVEN
 */
public class AlignmentProperty extends AbstractEnumProperty<Alignment> {
	private static final long serialVersionUID = -3105507615999461645L;

	public AlignmentProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public AlignmentProperty(String group, String alias, String name, String description, Alignment value) {
		super(group, alias, name, description, value);
	}
	
	/**
	 * @param source
	 */
	public AlignmentProperty(AlignmentProperty source) {
		super(source);
	}

	@Override
	public AlignmentProperty clone() {
		return new AlignmentProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	@Override
	public String getId() {
		return "Alignment";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	@Override
	public Class<Alignment> getValueType() {
		return Alignment.class;
	}
}
