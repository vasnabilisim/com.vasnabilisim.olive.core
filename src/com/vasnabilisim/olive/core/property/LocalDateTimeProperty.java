package com.vasnabilisim.olive.core.property;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

/**
 * LocalDateTime Property.
 * 
 * @author Menderes Fatih GUVEN
 */
public class LocalDateTimeProperty extends AbstractProperty<LocalDateTime> {
	private static final long serialVersionUID = 2666180710251766298L;

	public LocalDateTimeProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public LocalDateTimeProperty(String group, String alias, String name, String description, LocalDateTime value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public LocalDateTimeProperty(LocalDateTimeProperty source) {
		super(source);
	}
	
	@Override
	public LocalDateTimeProperty clone() {
		return new LocalDateTimeProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	public String getId() {
		return "LocalDateTime";
	}
	
	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	public Class<LocalDateTime> getValueType() {
		return LocalDateTime.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	public String getStringValue() {
		return value == null ? null : value.toString();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	public void setStringValue(String stringValue) {
		try {
			value = LocalDateTime.parse(stringValue);
		}
		catch (DateTimeParseException e) {
			value = LocalDateTime.now();
		}
	}
}
