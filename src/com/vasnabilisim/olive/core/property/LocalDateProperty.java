package com.vasnabilisim.olive.core.property;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

/**
 * LocalDate Property.
 * 
 * @author Menderes Fatih GUVEN
 */
public class LocalDateProperty extends AbstractProperty<LocalDate> {
	private static final long serialVersionUID = 6293208316671404897L;

	public LocalDateProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public LocalDateProperty(String group, String alias, String name, String description, LocalDate value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public LocalDateProperty(LocalDateProperty source) {
		super(source);
	}
	
	@Override
	public LocalDateProperty clone() {
		return new LocalDateProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	public String getId() {
		return "LocalDate";
	}
	
	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	public Class<LocalDate> getValueType() {
		return LocalDate.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	public String getStringValue() {
		return value == null ? null : value.toString();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	public void setStringValue(String stringValue) {
		try {
			value = LocalDate.parse(stringValue);
		}
		catch (DateTimeParseException e) {
			value = LocalDate.now();
		}
	}
}
