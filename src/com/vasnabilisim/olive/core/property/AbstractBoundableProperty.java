package com.vasnabilisim.olive.core.property;


/**
 * Integer Property.
 * @author Menderes Fatih GUVEN
 */
public abstract class AbstractBoundableProperty<N extends Comparable<? super N>> extends AbstractProperty<N> {
	private static final long serialVersionUID = 4159883609114344757L;

	protected N defaultValue = null;
	protected N minimumValue = null;
	protected N maximumValue = null;
	
	public AbstractBoundableProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 * @param minimumValue
	 * @param maximumValue
	 */
	public AbstractBoundableProperty(String group, String alias, String name, String description, N value, N minimumValue, N maximumValue) {
		super(group, alias, name, description, value);
		this.defaultValue = value;
		this.minimumValue = minimumValue;
		this.maximumValue = maximumValue;
	}

	/**
	 * @param source
	 */
	public AbstractBoundableProperty(AbstractBoundableProperty<N> source) {
		super(source);
		this.defaultValue = source.defaultValue;
		this.minimumValue = source.minimumValue;
		this.maximumValue = source.maximumValue;
	}

	public N getDefaultValue() {
		return defaultValue;
	}
	
	public N getMinimumValue() {
		return minimumValue;
	}
	
	public N getMaximumValue() {
		return maximumValue;
	}
	
	protected boolean validateValue() {
		if(value == null) {
			super.setValue(defaultValue);
			return false;
		}
		if(minimumValue != null && minimumValue.compareTo(value) > 0) {
			super.setValue(defaultValue);
			return false;
		}
		else if(maximumValue != null && maximumValue.compareTo(value) < 0) {
			super.setValue(defaultValue);
			return false;
		}
		return true;
	}
	
	@Override
	public N setValue(N value) {
		super.setValue(value);
		validateValue();
		return this.value;
	}
}
