package com.vasnabilisim.olive.core.property;

import com.vasnabilisim.olive.core.geom.DimensionPolicy;


/**
 * Dimension Policy Property.
 * @see com.vasnabilisim.olive.core.geom.DimensionPolicy
 * @author Menderes Fatih GUVEN
 */
public class DimensionPolicyProperty extends AbstractEnumProperty<DimensionPolicy> {
	private static final long serialVersionUID = 1549839836369284513L;

	public DimensionPolicyProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public DimensionPolicyProperty(String group, String alias, String name, String description, DimensionPolicy value) {
		super(group, alias, name, description, value);
	}
	
	/**
	 * @param source
	 */
	public DimensionPolicyProperty(DimensionPolicyProperty source) {
		super(source);
	}

	@Override
	public DimensionPolicyProperty clone() {
		return new DimensionPolicyProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	@Override
	public String getId() {
		return "DimensionPolicy";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	@Override
	public Class<DimensionPolicy> getValueType() {
		return DimensionPolicy.class;
	}
}
