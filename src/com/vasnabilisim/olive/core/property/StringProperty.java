package com.vasnabilisim.olive.core.property;

/**
 * String Property.
 * @author Menderes Fatih GUVEN
 */
public class StringProperty extends AbstractProperty<String> {
	private static final long serialVersionUID = -7891545562523300417L;

	public StringProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public StringProperty(String group, String alias, String name, String description, String value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public StringProperty(StringProperty source) {
		super(source);
	}

	@Override
	public StringProperty clone() {
		return new StringProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	@Override
	public String getId() {
		return "String";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	@Override
	public Class<String> getValueType() {
		return String.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	@Override
	public String getStringValue() {
		return value;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	@Override
	public void setStringValue(String stringValue) {
		value = stringValue;
	}
}
