package com.vasnabilisim.olive.core.property;

import java.util.EnumSet;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.vasnabilisim.olive.core.paint.Font;
import com.vasnabilisim.olive.core.paint.FontStyle;

/**
 * Font Property.
 * @author Menderes Fatih GUVEN
 */
public class FontProperty extends AbstractProperty<Font> {
	private static final long serialVersionUID = 8408091445134232171L;

	public FontProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public FontProperty(String group, String alias, String name, String description, Font value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public FontProperty(FontProperty source) {
		super(source);
	}
	
	@Override
	public FontProperty clone() {
		return new FontProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	public String getId() {
		return "Font";
	}
	
	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	public Class<Font> getValueType() {
		return Font.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	public String getStringValue() {
		return value == null ? null : value.toString();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	public void setStringValue(String stringValue) {
		value = stringValue == null ? new Font() : Font.valueOf(stringValue);
	}

	@Override
	public void writeXmlElement(Document document, Element element) {
		if(value != null) {
			createXmlElement(document, element, "Name", value.getName());
			createXmlElement(document, element, "Size", value.getSize());
			for(FontStyle style : value.getStyles())
				createXmlElement(document, element, "Style", style);
		}
	}
	
	@Override
	public void readXmlElement(Document document, Element element) throws PropertyException {
		String name = readXmlElement(document, element, "Name", Font.Default.getName());
		float size = readXmlElement(document, element, "Size", Font.Default.getSize());
		EnumSet<FontStyle> styles = readXmlElements(document, element, "Style", FontStyle.class);
		value = new Font(name, size, styles);
	}
}
