package com.vasnabilisim.olive.core.property;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;

/**
 * LocalTime Property.
 * 
 * @author Menderes Fatih GUVEN
 */
public class LocalTimeProperty extends AbstractProperty<LocalTime> {
	private static final long serialVersionUID = 2666180710251766298L;

	public LocalTimeProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public LocalTimeProperty(String group, String alias, String name, String description, LocalTime value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public LocalTimeProperty(LocalTimeProperty source) {
		super(source);
	}
	
	@Override
	public LocalTimeProperty clone() {
		return new LocalTimeProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	public String getId() {
		return "LocalTime";
	}
	
	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	public Class<LocalTime> getValueType() {
		return LocalTime.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	public String getStringValue() {
		return value == null ? null : value.toString();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	public void setStringValue(String stringValue) {
		try {
			value = LocalTime.parse(stringValue);
		}
		catch (DateTimeParseException e) {
			value = LocalTime.now();
		}
	}
}
