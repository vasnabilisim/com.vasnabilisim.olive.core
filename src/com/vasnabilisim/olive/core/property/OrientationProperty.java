package com.vasnabilisim.olive.core.property;

import com.vasnabilisim.olive.core.geom.Orientation;

/**
 * Orientation Property.
 * @author Menderes Fatih GUVEN
 */
public class OrientationProperty extends AbstractEnumProperty<Orientation> {
	private static final long serialVersionUID = -1369682401189272608L;

	public OrientationProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public OrientationProperty(String group, String alias, String name, String description, Orientation value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public OrientationProperty(OrientationProperty source) {
		super(source);
	}

	@Override
	public OrientationProperty clone() {
		return new OrientationProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	@Override
	public String getId() {
		return "Orientation";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	@Override
	public Class<Orientation> getValueType() {
		return Orientation.class;
	}
}
