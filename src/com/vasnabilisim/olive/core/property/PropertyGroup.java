package com.vasnabilisim.olive.core.property;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import java.util.TreeMap;

import com.vasnabilisim.util.Cloneable;

/**
 * @author Menderes Fatih GUVEN
 */
public class PropertyGroup implements Cloneable<PropertyGroup>, java.lang.Cloneable, Serializable {
	private static final long serialVersionUID = 1987445972811606811L;

	String name;
	TreeMap<String, Property<?>> propertyMap = new TreeMap<>();
	
	public PropertyGroup() {
	}

	public PropertyGroup(String name) {
		this.name = name;
	}
	
	public PropertyGroup(PropertyGroup source) {
		this.name = source.name;
		for(Property<?> property : source.propertyMap.values())
			this.propertyMap.put(property.getName(), property.clone());
	}

	@Override
	public PropertyGroup cloneObject() {
		return new PropertyGroup(this);
	}
	
	@Override
	protected Object clone() {
		return cloneObject();
	}
	
	@Override
	public String toString() {
		return name;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Collection<Property<?>> getProperties() {
		return propertyMap.values();
	}
	
	public <P extends Property<?>> P get(String propertyName, Class<P> propertyType) {
		return propertyType.cast(propertyMap.get(propertyName));
	}
	
	public Property<?> get(String propertyName) {
		return propertyMap.get(propertyName);
	}
	
	public Property<?> add(Property<?> property) {
		Objects.requireNonNull(property);
		if(!name.equals(property.getGroup()))
			return null;
		return propertyMap.put(property.getName(), property);
	}
	
	public void addAll(PropertyGroup propertyGroup) {
		Objects.requireNonNull(propertyGroup);
		for(Property<?> property : propertyGroup.propertyMap.values())
			this.propertyMap.put(property.getName(), property);
	}
	
	public Property<?> remove(String propertyName) {
		return propertyMap.remove(propertyName);
	}
	
	public Property<?> remove(Property<?> property) {
		Objects.requireNonNull(property);
		if(!name.equals(property.getGroup()))
			return null;
		return propertyMap.remove(property.getName());
	}

	public boolean contains(String propertyName) {
		return propertyMap.containsKey(propertyName);
	}
	
	public boolean contains(Property<?> property) {
		Objects.requireNonNull(property);
		if(!name.equals(property.getGroup()))
			return false;
		return propertyMap.containsKey(property.getName());
	}
	
}
