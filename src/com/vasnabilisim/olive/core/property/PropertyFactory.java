package com.vasnabilisim.olive.core.property;

import java.awt.image.BufferedImage;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.TreeMap;

import com.vasnabilisim.olive.core.geom.Alignment;
import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.geom.DimensionPolicy;
import com.vasnabilisim.olive.core.geom.HorizontalAlignment;
import com.vasnabilisim.olive.core.geom.Insets;
import com.vasnabilisim.olive.core.geom.Orientation;
import com.vasnabilisim.olive.core.geom.Point;
import com.vasnabilisim.olive.core.geom.Rectangle;
import com.vasnabilisim.olive.core.geom.VerticalAlignment;
import com.vasnabilisim.olive.core.paint.Border;
import com.vasnabilisim.olive.core.paint.Color;
import com.vasnabilisim.olive.core.paint.Font;
import com.vasnabilisim.olive.core.paint.ImagePaintPolicy;
import com.vasnabilisim.olive.core.paper.Paper;

/**
 * Property factory class. 
 * This type is singleton. 
 * Use getInstance method to get the only instance of this type.
 * @author Menderes Fatih GUVEN
 */
public class PropertyFactory {

	/**
	 * One and only instance.
	 */
	protected static PropertyFactory instance = new PropertyFactory();
	
	/**
	 * Returns the instance.
	 * @return
	 */
	public static PropertyFactory getInstance() {
		return instance;
	}
	
	/**
	 * Sets the instance.
	 * @param instance
	 */
	public static void setInstance(PropertyFactory instance) {
		PropertyFactory.instance = instance;
	}
	
	protected PropertyFactory() {
		register("Alignment", AlignmentProperty.class);
		register("Boolean", BooleanProperty.class);
		register("Border", BorderProperty.class);
		register("Color", ColorProperty.class);
		register("Dimension", DimensionProperty.class);
		register("DimensionPolicy", DimensionPolicyProperty.class);
		register("Double", DoubleProperty.class);
		register("Float", FloatProperty.class);
		register("Font", FontProperty.class);
		register("HorizontalAlignment", HorizontalAlignmentProperty.class);
		register("Image", ImageProperty.class);
		register("ImagePaintPolicy", ImagePaintPolicyProperty.class);
		register("Insets", InsetsProperty.class);
		register("Integer", IntegerProperty.class);
		register("LocalDateTime", LocalDateTimeProperty.class);
		register("LocalDate", LocalDateProperty.class);
		register("LocalTime", LocalTimeProperty.class);
		register("Orientation", OrientationProperty.class);
		register("Page", PaperProperty.class);
		register("Point", PointProperty.class);
		register("Rectangle", RectangleProperty.class);
		register("String", StringProperty.class);
		register("VerticalAlignment", VerticalAlignmentProperty.class);
	}
	
	/**
	 * Registered property classes.
	 */
	protected TreeMap<String, Class<? extends Property<?>>> propertyMap = new TreeMap<>();
	
	/**
	 * Registers the property type to application. 
	 * Registration id must be same to 
	 * the result of getName method of property of given type.
	 * @param id Registration id
	 * @param propertyClass
	 */
	public void register(String id, Class<? extends Property<?>> propertyClass) {
		propertyMap.put(id, propertyClass);
	}
	
	/**
	 * Returs the registered property class which has given id.
	 * @param id
	 * @return
	 */
	public Class<? extends Property<?>> getPropertyClass(String id) {
		return propertyMap.get(id);
	}
	
	/**
	 * Creates a property of given id type and given name. 
	 * Sets the property value as string.
	 * @param id
	 * @param group
	 * @param name
	 * @param value
	 * @return
	 * @throws PropertyException
	 */
	public Property<?> createProperty(String id, String group, String name, String value) throws PropertyException {
		if(id == null) {
			throw new PropertyException("Null Id.");
		}
		if(name == null) {
			throw new PropertyException("Null key.");
		}
		Class<? extends Property<?>> propertyClass = getPropertyClass(id);
		if(propertyClass == null) {
			throw new PropertyException(String.format("Unable to find property type identified by [%s].", id));
		}
		Constructor<? extends Property<?>> constructor = null;
		try {
			constructor = propertyClass.getConstructor(new Class[] {});
		}
		catch(	SecurityException | 
				NoSuchMethodException e) 
		{
			throw new PropertyException(String.format("Property type [%s] is not valid. Check key constructor.", propertyClass.getName()));
		}
		
		Property<?> property = null;
		try {
			property = constructor.newInstance(new Object[] {});
		}
		catch(	IllegalArgumentException | 
				IllegalAccessException | 
				InstantiationException | 
				InvocationTargetException e) 
		{
			throw new PropertyException(String.format("Property type [%s] is not valid. Check key constructor.", propertyClass.getName()));
		}
		property.setGroup(group);
		property.setName(name);
		property.setStringValue(value);
		return property;
	}

	public static BooleanProperty createBooleanProperty(String group, String title, String name, String description, Boolean value) {
		return new BooleanProperty(group, title, name, description, value);
	}

	public static BorderProperty createBorderProperty(String group, String title, String name, String description, Border value) {
		return new BorderProperty(group, title, name, description, value);
	}

	public static ColorProperty createColorProperty(String group, String title, String name, String description, Color value) {
		return new ColorProperty(group, title, name, description, value);
	}

	public static FontProperty createFontProperty(String group, String title, String name, String description, Font value) {
		return new FontProperty(group, title, name, description, value);
	}

	public static InsetsProperty createInsetsProperty(String group, String title, String name, String description, Insets value) {
		return new InsetsProperty(group, title, name, description, value);
	}

	public static ImageProperty createImageProperty(String group, String title, String name, String description, BufferedImage value) {
		return new ImageProperty(group, title, name, description, value);
	}

	public static ImagePaintPolicyProperty createImagePaintPolicyProperty(String group, String title, String name, String description, ImagePaintPolicy value) {
		return new ImagePaintPolicyProperty(group, title, name, description, value);
	}

	public static IntegerProperty createIntegerProperty(String group, String title, String name, String description, Integer value) {
		return new IntegerProperty(group, title, name, description, value);
	}

	public static IntegerProperty createIntegerProperty(String group, String title, String name, String description, Integer value, Integer minimumValue, Integer maximumValue) {
		return new IntegerProperty(group, title, name, description, value, minimumValue, maximumValue);
	}

	public static LongProperty createLongProperty(String group, String title, String name, String description, Long value) {
		return new LongProperty(group, title, name, description, value);
	}

	public static LongProperty createLongProperty(String group, String title, String name, String description, Long value, Long minimumValue, Long maximumValue) {
		return new LongProperty(group, title, name, description, value, minimumValue, maximumValue);
	}

	public static FloatProperty createFloatProperty(String group, String title, String name, String description, Float value) {
		return new FloatProperty(group, title, name, description, value);
	}

	public static FloatProperty createFloatProperty(String group, String title, String name, String description, Float value, Float minimumValue, Float maximumValue) {
		return new FloatProperty(group, title, name, description, value, minimumValue, maximumValue);
	}

	public static DoubleProperty createDoubleProperty(String group, String title, String name, String description, Double value) {
		return new DoubleProperty(group, title, name, description, value);
	}

	public static DoubleProperty createDoubleProperty(String group, String title, String name, String description, Double value, Double minimumValue, Double maximumValue) {
		return new DoubleProperty(group, title, name, description, value, minimumValue, maximumValue);
	}

	public static LocalDateTimeProperty createLocalDateTimeProperty(String group, String title, String name, String description, LocalDateTime value) {
		return new LocalDateTimeProperty(group, title, name, description, value);
	}

	public static LocalDateProperty createLocalDateProperty(String group, String title, String name, String description, LocalDate value) {
		return new LocalDateProperty(group, title, name, description, value);
	}

	public static LocalTimeProperty createLocalTimeProperty(String group, String title, String name, String description, LocalTime value) {
		return new LocalTimeProperty(group, title, name, description, value);
	}

	public static PointProperty createPointProperty(String group, String title, String name, String description, Point value) {
		return new PointProperty(group, title, name, description, value);
	}

	public static DimensionProperty createDimensionProperty(String group, String title, String name, String description, Dimension value) {
		return new DimensionProperty(group, title, name, description, value);
	}

	public static DimensionPolicyProperty createDimensionPolicyProperty(String group, String title, String name, String description, DimensionPolicy value) {
		return new DimensionPolicyProperty(group, title, name, description, value);
	}

	public static RectangleProperty createRectangleProperty(String group, String title, String name, String description, Rectangle value) {
		return new RectangleProperty(group, title, name, description, value);
	}

	public static StringProperty createStringProperty(String group, String title, String name, String description, String value) {
		return new StringProperty(group, title, name, description, value);
	}

	public static AlignmentProperty createAlignmentProperty(String group, String title, String name, String description, Alignment value) {
		return new AlignmentProperty(group, title, name, description, value);
	}

	public static HorizontalAlignmentProperty createHorizontalAlignmentProperty(String group, String title, String name, String description, HorizontalAlignment value) {
		return new HorizontalAlignmentProperty(group, title, name, description, value);
	}

	public static VerticalAlignmentProperty createVerticalAlignmentProperty(String group, String title, String name, String description, VerticalAlignment value) {
		return new VerticalAlignmentProperty(group, title, name, description, value);
	}

	public static OrientationProperty createOrientationProperty(String group, String title, String name, String description, Orientation value) {
		return new OrientationProperty(group, title, name, description, value);
	}

	public static PaperProperty createPaperProperty(String group, String title, String name, String description, Paper value) {
		return new PaperProperty(group, title, name, description, value);
	}
}
