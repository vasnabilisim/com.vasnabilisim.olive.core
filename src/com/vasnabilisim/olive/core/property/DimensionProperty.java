package com.vasnabilisim.olive.core.property;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.vasnabilisim.olive.core.geom.Dimension;

/**
 * Dimension Property.
 * 
 * @author Menderes Fatih GUVEN
 */
public class DimensionProperty extends AbstractProperty<Dimension> {
	private static final long serialVersionUID = -126124740130003663L;

	public DimensionProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public DimensionProperty(String group, String alias, String name, String description, Dimension value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public DimensionProperty(DimensionProperty source) {
		super(source);
	}

	@Override
	public DimensionProperty clone() {
		return new DimensionProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	public String getId() {
		return "Dimension";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	public Class<Dimension> getValueType() {
		return Dimension.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	public String getStringValue() {
		return value == null ? null : value.toString();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	public void setStringValue(String stringValue) {
		value = Dimension.valueOf(stringValue);
	}
	
	@Override
	public void writeXmlElement(Document document, Element element) {
		if(value != null) {
			createXmlElement(document, element, "Width", value.w);
			createXmlElement(document, element, "Height", value.h);
		}
	}
	
	@Override
	public void readXmlElement(Document document, Element element) throws PropertyException {
		value = Dimension.Zero.clone();
		value.w = readXmlElement(document, element, "Width", value.w);
		value.h = readXmlElement(document, element, "Height", value.h);
	}
}
