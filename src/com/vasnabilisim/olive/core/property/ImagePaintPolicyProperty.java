package com.vasnabilisim.olive.core.property;

import com.vasnabilisim.olive.core.paint.ImagePaintPolicy;


/**
 * Image Paint Policy Property.
 * @see com.vasnabilisim.olive.core.geom.Alignment
 * @author Menderes Fatih GUVEN
 */
public class ImagePaintPolicyProperty extends AbstractEnumProperty<ImagePaintPolicy> {
	private static final long serialVersionUID = -7472988788484444901L;

	public ImagePaintPolicyProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public ImagePaintPolicyProperty(String group, String alias, String name, String description, ImagePaintPolicy value) {
		super(group, alias, name, description, value);
	}
	
	/**
	 * @param source
	 */
	public ImagePaintPolicyProperty(ImagePaintPolicyProperty source) {
		super(source);
	}

	@Override
	public ImagePaintPolicyProperty clone() {
		return new ImagePaintPolicyProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	@Override
	public String getId() {
		return "ImagePaintPolicy";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	@Override
	public Class<ImagePaintPolicy> getValueType() {
		return ImagePaintPolicy.class;
	}
}
