package com.vasnabilisim.olive.core.property;

import java.io.Serializable;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * General use property interface.
 * 
 * @author Menderes Fatih GUVEN
 */
public interface Property<V> extends Cloneable, Serializable {

	Property<V> clone();
	
	/**
	 * Returns the unique id of property type.
	 * 
	 * @return
	 */
	String getId();

	/**
	 * Returns the property value type.
	 * 
	 * @return
	 */
	Class<V> getValueType();
	
	/**
	 * Group of the property.
	 * 
	 * @return
	 */
	String getGroup();
	
	/**
	 * Sets the group.
	 * 
	 * @param group
	 */
	void setGroup(String group);

	/**
	 * Alias of the property.
	 * 
	 * @return
	 */
	String getAlias();
	
	/**
	 * Sets the alias.
	 * 
	 * @param alias
	 */
	void setAlias(String alias);

	/**
	 * Name of the property.
	 * 
	 * @return
	 */
	String getName();
	
	/**
	 * Sets the name.
	 * 
	 * @param name
	 */
	void setName(String name);

	/**
	 * Returns a description about the property.
	 * 
	 * @return
	 */
	String getDescription();
	
	/**
	 * Sets the description.
	 * 
	 * @param description
	 */
	void setDescription(String description);

	/**
	 * Returns the value of the property.
	 * 
	 * @return
	 */
	V getValue();

	/**
	 * Sets the value of the property.
	 * 
	 * @param value
	 * @return
	 */
	V setValue(V value);
	
	/**
	 * Sets the value of the property. 
	 * Uses value of the given property. 
	 * 
	 * @param property
	 * @return
	 */
	Property<V> setValue(Property<V> property);
	
	/**
	 * Sets the value of the property.
	 * 
	 * @param value
	 * @return
	 */
	V setObjectValue(Object value);

	/**
	 * Returns the string representation of the value.
	 * 
	 * @return
	 */
	String getStringValue();

	/**
	 * Sets the string representation of the value.
	 * 
	 * @param stringValue
	 * @throws PropertyException
	 */
	void setStringValue(String stringValue) throws PropertyException;
	
	/**
	 * Sets stringValue to text context of given element.
	 * @param document
	 * @param element
	 * @throws PropertyException
	 */
	void writeXmlElement(Document document, Element element) throws PropertyException;
	
	/**
	 * Sets text context of given element to stringValue.
	 * @param document
	 * @param element
	 * @throws PropertyException
	 */
	void readXmlElement(Document document, Element element) throws PropertyException;

	/**
	 * Appends property to given element as an attribute.
	 * @param document
	 * @param element
	 * @throws PropertyException
	 */
	void writeXmlAttribute(Document document, Element element) throws PropertyException;
	
	/**
	 * Reads property value from the corresponding attribute of given element.
	 * @param document
	 * @param element
	 * @throws PropertyException
	 */
	void readXmlAttribute(Document document, Element element) throws PropertyException;

	public static class VoidProperty extends AbstractProperty<Void> {
		private static final long serialVersionUID = -7758861896482748682L;
		public String getId() {return "Void";}
		public Class<Void> getValueType() {return Void.class;}
		public String getStringValue() {return null;}
		public void setStringValue(String stringValue) {}
		public VoidProperty clone() {return this;}
	}
	
	/**
	 * Void property.
	 */
	VoidProperty VOID_PROPERTY = new VoidProperty();
}
