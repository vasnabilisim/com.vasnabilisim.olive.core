package com.vasnabilisim.olive.core.property;


/**
 * Integer Property.
 * @author Menderes Fatih GUVEN
 */
public class IntegerProperty extends AbstractBoundableProperty<Integer> {
	private static final long serialVersionUID = 5426083449199708942L;

	public IntegerProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public IntegerProperty(String group, String alias, String name, String description, Integer value) {
		this(group, alias, name, description, value, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 * @param minimunValue
	 * @param maximumValue
	 */
	public IntegerProperty(String group, String alias, String name, String description, Integer value, Integer minimunValue, Integer maximumValue) {
		super(group, alias, name, description, value, minimunValue, maximumValue);
	}

	/**
	 * @param source
	 */
	public IntegerProperty(IntegerProperty source) {
		super(source);
	}
	
	@Override
	public IntegerProperty clone() {
		return new IntegerProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	public String getId() {
		return "Integer";
	}
	
	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	public Class<Integer> getValueType() {
		return Integer.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	public String getStringValue() {
		if(value == null)
			return null;
		return value.toString();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	public void setStringValue(String stringValue) {
		try {
			value = Integer.valueOf(stringValue);
		}
		catch (NumberFormatException e) {
			value = 0;
		}
	}
}
