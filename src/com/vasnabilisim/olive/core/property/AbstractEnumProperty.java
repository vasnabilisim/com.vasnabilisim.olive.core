package com.vasnabilisim.olive.core.property;


/**
 * Abstract Enum Property .
 * @author Menderes Fatih GUVEN
 */
public abstract class AbstractEnumProperty<E extends Enum<E>> extends AbstractProperty<E> {
	private static final long serialVersionUID = -7922486201993461437L;

	/**
	 * Default constructor.
	 */
	public AbstractEnumProperty() {
		super();
	}

	/**
	 * Group constructor
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public AbstractEnumProperty(String group, String alias, String name, String description, E value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public AbstractEnumProperty(AbstractEnumProperty<E> source) {
		super(source);
	}
	
	@Override
	public abstract AbstractEnumProperty<E> clone();

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	@Override
	public String getStringValue() {
		return value == null ? null : value.name();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	@Override
	public void setStringValue(String stringValue) {
		try {
			value = Enum.valueOf(getValueType(), stringValue);
		} catch (Exception e) {
			value = getValueType().getEnumConstants()[0];
		}
	}
}
