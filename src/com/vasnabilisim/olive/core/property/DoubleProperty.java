package com.vasnabilisim.olive.core.property;


/**
 * Double Property.
 * @author Menderes Fatih GUVEN
 */
public class DoubleProperty extends AbstractBoundableProperty<Double> {
	private static final long serialVersionUID = -8162662228424700311L;

	public DoubleProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public DoubleProperty(String group, String alias, String name, String description, Double value) {
		super(group, alias, name, description, value, -Double.MAX_VALUE, Double.MAX_VALUE);
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 * @param minimumValue
	 * @param maximumValue
	 */
	public DoubleProperty(String group, String alias, String name, String description, Double value, Double minimumValue, Double maximumValue) {
		super(group, alias, name, description, value, minimumValue, maximumValue);
	}

	/**
	 * @param source
	 */
	public DoubleProperty(DoubleProperty source) {
		super(source);
	}

	@Override
	public DoubleProperty clone() {
		return new DoubleProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	@Override
	public String getId() {
		return "Double";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	@Override
	public Class<Double> getValueType() {
		return Double.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	@Override
	public String getStringValue() {
		if(value == null)
			return null;
		return value.toString();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	@Override
	public void setStringValue(String stringValue) {
		try {
			value = Double.valueOf(stringValue);
		}
		catch (NumberFormatException e) {
			value = new Double(0.0);
		}
	}
}
