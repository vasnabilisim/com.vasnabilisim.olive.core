package com.vasnabilisim.olive.core.property;


/**
 * Long Property.
 * 
 * @author Menderes Fatih GUVEN
 */
public class LongProperty extends AbstractBoundableProperty<Long> {
	private static final long serialVersionUID = 6337544534964303667L;

	public LongProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public LongProperty(String group, String alias, String name, String description, Long value) {
		super(group, alias, name, description, value, Long.MIN_VALUE, Long.MAX_VALUE);
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 * @param minimunValue
	 * @param maximumValue
	 */
	public LongProperty(String group, String alias, String name, String description, Long value, Long minimunValue, Long maximumValue) {
		super(group, alias, name, description, value, minimunValue, maximumValue);
	}

	/**
	 * @param source
	 */
	public LongProperty(LongProperty source) {
		super(source);
	}
	
	@Override
	public LongProperty clone() {
		return new LongProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	public String getId() {
		return "Long";
	}
	
	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	public Class<Long> getValueType() {
		return Long.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	public String getStringValue() {
		if(value == null)
			return null;
		return value.toString();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	public void setStringValue(String stringValue) {
		try {
			value = Long.valueOf(stringValue);
		}
		catch (NumberFormatException e) {
			value = 0l;
		}
	}
}
