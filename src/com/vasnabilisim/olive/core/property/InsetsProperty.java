package com.vasnabilisim.olive.core.property;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.vasnabilisim.olive.core.geom.Insets;

/**
 * Insets Property.
 * 
 * @author Menderes Fatih GUVEN
 */
public class InsetsProperty extends AbstractProperty<Insets> {
	private static final long serialVersionUID = -1633050371458607316L;

	public InsetsProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public InsetsProperty(String group, String alias, String name, String description, Insets value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public InsetsProperty(InsetsProperty source) {
		super(source);
	}

	@Override
	public InsetsProperty clone() {
		return new InsetsProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	public String getId() {
		return "Insets";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	public Class<Insets> getValueType() {
		return Insets.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	public String getStringValue() {
		return value == null ? null : value.toString();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	public void setStringValue(String stringValue) {
		value = Insets.valueOf(stringValue);
	}

	@Override
	public void writeXmlElement(Document document, Element element) {
		if(value != null) {
			createXmlElement(document, element, "Left", value.l);
			createXmlElement(document, element, "Top", value.t);
			createXmlElement(document, element, "Right", value.r);
			createXmlElement(document, element, "Bottom", value.b);
		}
	}
	
	@Override
	public void readXmlElement(Document document, Element element) throws PropertyException {
		value = Insets.Zero.clone();
		value.l = readXmlElement(document, element, "Left", value.l);
		value.t = readXmlElement(document, element, "Top", value.t);
		value.r = readXmlElement(document, element, "Right", value.r);
		value.b = readXmlElement(document, element, "Bottom", value.b);
	}
}
