package com.vasnabilisim.olive.core.property;

import com.vasnabilisim.olive.core.paint.Color;

/**
 * Color Property.
 * @author Menderes Fatih GUVEN
 */
public class ColorProperty extends AbstractProperty<Color> {
	private static final long serialVersionUID = -7326399149144902073L;

	public ColorProperty() {
		super();
	}

	/**
	 * @param group
	 * @param alias
	 * @param name
	 * @param description
	 * @param value
	 */
	public ColorProperty(String group, String alias, String name, String description, Color value) {
		super(group, alias, name, description, value);
	}

	/**
	 * @param source
	 */
	public ColorProperty(ColorProperty source) {
		super(source);
	}

	@Override
	public ColorProperty clone() {
		return new ColorProperty(this);
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getId()
	 */
	public String getId() {
		return "Color";
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getValueType()
	 */
	public Class<Color> getValueType() {
		return Color.class;
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#getStringValue()
	 */
	public String getStringValue() {
		return value == null ? null : value.toString();
	}

	/**
	 * @see com.vasnabilisim.olive.core.property.Property#setStringValue(java.lang.String)
	 */
	public void setStringValue(String stringValue) {
		value = Color.valueOf(stringValue);
	}
}
