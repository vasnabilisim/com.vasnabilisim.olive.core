package com.vasnabilisim.olive.core.paint;

import java.io.Serializable;

/**
 * @author Menderes Fatih GUVEN
 */
public interface Paint<P extends Paint<P>> extends Cloneable, Serializable {
	P clone();
}
