package com.vasnabilisim.olive.core.paint;

/**
 * @author Menderes Fatih GUVEN
 */
public enum ImagePaintPolicy {

	Single, 
	RepeatX, 
	RepeatY, 
	Repeat, 
	FitX, 
	FitY, 
	Fit
}
