package com.vasnabilisim.olive.core.paint;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumSet;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * @author Menderes Fatih GUVEN
 */
public class Font implements com.vasnabilisim.util.Cloneable<Font>, Cloneable, Serializable {
	private static final long serialVersionUID = 6084003870463960969L;

	private static final String DEFAULT_FAMILY = "System";
	private static final float DEFAULT_SIZE = 8f;
	public static final Font Default = new Font();
	
	String name;
	float size;
	EnumSet<FontStyle> styles;

	/**
	 * Default constructor.
	 */
	public Font() {
		this.name = DEFAULT_FAMILY;
		this.size = DEFAULT_SIZE;
		this.styles = EnumSet.noneOf(FontStyle.class);
	}

	/**
	 * Value constructor.
	 * @param name
	 * @param size
	 * @param styles
	 */
	public Font(String name, float size, FontStyle... styles) {
		this.name = name;
		this.size = size;
		this.styles = EnumSet.noneOf(FontStyle.class);
		Collections.addAll(this.styles, styles);
	}

	/**
	 * Value constructor.
	 * @param name
	 * @param size
	 * @param styles
	 */
	public Font(String name, float size, EnumSet<FontStyle> styles) {
		this.name = name;
		this.size = size;
		this.styles = styles == null ? EnumSet.noneOf(FontStyle.class) : styles;
	}

	/**
	 * Copy constructor.
	 * @param source
	 */
	public Font(Font source) {
		this.name = source.name;
		this.size = source.size;
		this.styles = EnumSet.noneOf(FontStyle.class);
		this.styles.addAll(source.styles);
	}

	/**
	 * @see com.vasnabilisim.util.Cloneable#cloneObject()
	 */
	@Override
	public Font cloneObject() {
		return new Font(this);
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		return cloneObject();
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public float getSize() {
		return size;
	}
	
	public void setSize(float size) {
		this.size = size;
	}
	
	public EnumSet<FontStyle> getStyles() {
		return styles;
	}
	
	public boolean hasStyle(FontStyle style) {
		return styles.contains(style);
	}
	
	public void addStyle(FontStyle style) {
		styles.add(style);
	}
	
	public void removeStyle(FontStyle style) {
		styles.add(style);
	}
	
	public void setStyles(FontStyle... styles) {
		this.styles.clear();
		Collections.addAll(this.styles, styles);
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(100);
		builder.append(name);
		builder.append(",").append(size);
		for(FontStyle style : styles)
			builder.append(",").append(style.name());
		return builder.toString();
	}

	/**
	 * Returns a Font instance represented by given string value.
	 * @param stringValue
	 * @return
	 */
	public static Font valueOf(String stringValue) {
		if (stringValue == null)
			return new Font();
		try {
			StringTokenizer tokenizer = new StringTokenizer(stringValue, ",", false);

			Font font = new Font(tokenizer.nextToken(), Float.parseFloat(tokenizer.nextToken()));
			
			while(tokenizer.hasMoreTokens())
				font.addStyle(Enum.valueOf(FontStyle.class, tokenizer.nextToken()));
			
			return font;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			return new Font();
		}
	}
}
