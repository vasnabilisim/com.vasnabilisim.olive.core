package com.vasnabilisim.olive.core.paint;

/**
 * @author Menderes Fatih GUVEN
 */
public abstract class AbstractPaint<AP extends AbstractPaint<AP>> implements Paint<AP> {
	private static final long serialVersionUID = -487390414914179396L;
	
	@Override
	public abstract AP clone();
}
