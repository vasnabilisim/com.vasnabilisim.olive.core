package com.vasnabilisim.olive.core.paint;


/**
 * @author mfg
 */
public class Color extends AbstractPaint<Color> {
    private static final long serialVersionUID = 2321542717569738390L;

    public float r = 0f;
    public float g = 0f;
    public float b = 0f;
    public float o = 1f;
    
    public static Color Transparent = new Color(0f, 0f, 0f, 0f);
    public static Color White = new Color(1f, 1f, 1f, 1f);
    public static Color LightGray = new Color(.75f, .75f, .75f, 1f);
    public static Color Gray = new Color(.5f, .5f, .5f, 1f);
    public static Color DarkGray = new Color(.25f, .25f, .25f, 1f);
    public static Color Black = new Color(0f, 0f, 0f, 1f);
    public static Color Red = new Color(1f, 0f, 0f, 1f);
    public static Color Yellow = new Color(1f, 1f, 0f, 1f);
    public static Color Green = new Color(0f, 1f, 0f, 1f);
    public static Color Cyan = new Color(0f, 1f, 1f, 1f);
    public static Color Blue = new Color(0f, 0f, 1f, 1f);
    public static Color Purple = new Color(1f, 0f, 1f, 1f);

    /**
	 * Default constructor. 
	 * Constructs black.
	 */
	public Color() {
	}

	/**
	 * Value constructor. 
	 * Creates an new color instance with given values
	 * @param red [0,1]
	 * @param green [0,1]
	 * @param blue [0,1]
	 */
	public Color(float red, float green, float blue) {
		this(red, green, blue, 1f);
	}

	/**
	 * Value constructor. 
	 * Creates an new color instance with given values
	 * @param red [0,1]
	 * @param green [0,1]
	 * @param blue [0,1]
	 * @param opacity [0,1]
	 */
	public Color(float red, float green, float blue, float opacity) {
        if (red < 0f || red > 1f)
            throw new IllegalArgumentException("Red value (" + red + ") must be in the range 0.0-1.0");
        if (green < 0f || green > 1f)
            throw new IllegalArgumentException("Green value (" + green + ") must be in the range 0.0-1.0");
        if (blue < 0f || blue > 1f)
            throw new IllegalArgumentException("Blue value (" + blue + ") must be in the range 0.0-1.0");
        if (opacity < 0 || opacity > 1)
            throw new IllegalArgumentException("Opacity value (" + opacity + ") must be in the range 0.0-1.0");
		this.r = red;
		this.g= green;
		this.b = blue;
		this.o = opacity;
	}

	/**
	 * Default constructor.
	 * @param source
	 */
	public Color(Color source) {
		this.r = source.r;
		this.g= source.g;
		this.b = source.b;
		this.o = source.o;
	}

	/**
	 * @see com.vasnabilisim.olive.core.paint.AbstractPaint#clone()
	 */
	@Override
	public Color clone() {
		return new Color(this);
	}
	
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(this == obj)
			return true;
		if(!(obj instanceof Color))
			return false;
		Color other = (Color) obj;
		return	this.r == other.r && 
				this.g == other.g && 
				this.b == other.b && 
				this.o == other.o;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("#%02x%02x%02x%02x", (int)(r*255f), (int)(g*255f), (int)(b*255f), (int)(o*255f));
		/*
		StringBuilder builder = new StringBuilder(100);
		builder.append(r);
		builder.append(",").append(g);
		builder.append(",").append(b);
		builder.append(",").append(o);
		return builder.toString();
		*/
	}
	
	/**
	 * Returns a Color instance represented by given string value.
	 * @param stringValue
	 * @return
	 */
	public static Color valueOf(String stringValue) {
		if (stringValue == null || stringValue.length() == 0)
			return Black;
		if(stringValue.charAt(0) == '#') {
			float r = 0;
			float g = 0;
			float b = 0;
			float o = 1;
			int length = stringValue.length();
			try {
				if(length >= 3)
					r = Integer.parseInt(stringValue.substring(1, 3), 16) / 255f;
				if(length >= 5)
					g = Integer.parseInt(stringValue.substring(3, 5), 16) / 255f;
				if(length >= 7)
					b = Integer.parseInt(stringValue.substring(5, 7), 16) / 255f;
				if(length >= 9)
					o = Integer.parseInt(stringValue.substring(7, 9), 16) / 255f;
				return new Color(r, g, b, o);
			} catch (NumberFormatException e) {
				return Black;
			}
		}
		//Other formats can be implemented
		return Black;

		/*
		try {
			StringTokenizer tokenizer = new StringTokenizer(stringValue, ",", false);
			String r = tokenizer.nextToken();
			String g = tokenizer.nextToken();
			String b = tokenizer.nextToken();
			String o = tokenizer.nextToken();

			float rValue = java.lang.Float.parseFloat(r);
			float gValue = java.lang.Float.parseFloat(g);
			float bValue = java.lang.Float.parseFloat(b);
			float oValue = java.lang.Float.parseFloat(o);

			return new Color(rValue, gValue, bValue, oValue);
		} catch (NoSuchElementException | NumberFormatException e) {
			return new Color(0f, 0f, 0f, 0f);
		}
		*/
	}
}
