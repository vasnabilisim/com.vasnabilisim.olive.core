package com.vasnabilisim.olive.core.paint;

import java.io.Serializable;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import com.vasnabilisim.olive.core.geom.Insets;


/**
 * @author Menderes Fatih GUVEN
 */
public class Border implements Cloneable, Serializable {
	private static final long serialVersionUID = -4975481425536254754L;

	public static Border Empty = new Border(0f, Color.Transparent.clone());
	
	public Insets thickness;
	
    public Color lColor;
    public Color tColor;
    public Color rColor;
    public Color bColor;

	public Border() {
		this(1f, Color.Black);
	}

	public Border(float allThickness, Color allColor) {
		thickness = new Insets(allThickness);
		lColor = allColor.clone();
		tColor = allColor.clone();
		rColor = allColor.clone();
		bColor = allColor.clone();
	}

	public Border(
		    float lThickness, 
		    Color lColor, 
		    float tThickness, 
		    Color tColor, 
		    float rThickness, 
		    Color rColor, 
		    float bThickness, 
		    Color bColor) 
	{
		this.thickness = new Insets(lThickness, tThickness, rThickness, bThickness);
	    this.lColor = lColor;
	    this.tColor = tColor;
	    this.rColor = rColor;
	    this.bColor = bColor;
	}
	
	public Border(Border source) {
	    this.thickness = source.thickness.clone();
	    this.lColor = source.lColor == null ? Color.Transparent.clone() : source.lColor.clone();
	    this.tColor = source.tColor == null ? Color.Transparent.clone() : source.tColor.clone();
	    this.rColor = source.rColor == null ? Color.Transparent.clone() : source.rColor.clone();
	    this.bColor = source.bColor == null ? Color.Transparent.clone() : source.bColor.clone();
	}

	@Override
	public Border clone() {
		return new Border(this);
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(100);
		builder.append(thickness.l).append(',').append(lColor);
		builder.append(",").append(thickness.t).append(',').append(tColor);
		builder.append(",").append(thickness.r).append(',').append(rColor);
		builder.append(",").append(thickness.b).append(',').append(bColor);
		return builder.toString();
	}
	
	/**
	 * Returns a Border instance represented by given string value.
	 * @param stringValue
	 * @return
	 */
	public static Border valueOf(String stringValue) {
		if(stringValue == null)
			return Empty.clone();
		try {
			StringTokenizer tokenizer = new StringTokenizer(stringValue, ",", false);
			return new Border(
					java.lang.Float.parseFloat(tokenizer.nextToken()), 
					Color.valueOf(tokenizer.nextToken()), 
					java.lang.Float.parseFloat(tokenizer.nextToken()), 
					Color.valueOf(tokenizer.nextToken()), 
					java.lang.Float.parseFloat(tokenizer.nextToken()), 
					Color.valueOf(tokenizer.nextToken()), 
					java.lang.Float.parseFloat(tokenizer.nextToken()), 
					Color.valueOf(tokenizer.nextToken()));
		} catch (NoSuchElementException | NumberFormatException e) {
			return Empty.clone();
		}
	}
}
