package com.vasnabilisim.olive.core.paint;

/**
 * @author Menderes Fatih GUVEN
 */
public enum FontStyle {

	Bold, 
	Italic, 
	Underline, 
	Overline;
}
