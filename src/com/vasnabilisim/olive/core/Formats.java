package com.vasnabilisim.olive.core;

/**
 * @author Menderes Fatih GUVEN
 */
public final class Formats {

	private Formats() {}
	
	public static java.time.format.DateTimeFormatter LocalDateTimeFormatter = java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
	public static java.time.LocalDateTime parseLocalDateTime(String value) {
		return value == null ? null : java.time.LocalDateTime.parse(value, LocalDateTimeFormatter);
	}
	public static String formatLocalDateTime(java.time.LocalDateTime value) {
		return value == null ? null : value.format(LocalDateTimeFormatter);
	}
	
	public static java.time.format.DateTimeFormatter LocalDateFormatter = java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd");
	public static java.time.LocalDate parseLocalDate(String value) {
		return value == null ? null : java.time.LocalDate.parse(value, LocalDateFormatter);
	}
	public static String formatLocalDate(java.time.LocalDate value) {
		return value == null ? null : value.format(LocalDateFormatter);
	}
	
	public static java.time.format.DateTimeFormatter LocalTimeFormatter = java.time.format.DateTimeFormatter.ofPattern("HH:mm:ss.SSS");
	public static java.time.LocalTime parseLocalTime(String value) {
		return value == null ? null : java.time.LocalTime.parse(value, LocalTimeFormatter);
	}
	public static String formatLocalTime(java.time.LocalTime value) {
		return value == null ? null : value.format(LocalTimeFormatter);
	}

	
	public static java.text.SimpleDateFormat SqlTimestampFormat = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
	public static java.sql.Timestamp parseSqlTimestamp(String value) {
		try {
			return value == null ? null : new java.sql.Timestamp(SqlTimestampFormat.parse(value).getTime());
		} catch (java.text.ParseException e) {
			return null;
		}
	}
	public static String formatSqlTimestamp(java.sql.Timestamp value) {
		return value == null ? null : SqlTimestampFormat.format(value);
	}

	public static java.text.SimpleDateFormat SqlDateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
	public static java.sql.Date parseSqlDate(String value) {
		try {
			return value == null ? null : new java.sql.Date(SqlDateFormat.parse(value).getTime());
		} catch (java.text.ParseException e) {
			return null;
		}
	}
	public static String formatSqlDate(java.sql.Date value) {
		return value == null ? null : SqlDateFormat.format(value);
	}

	public static java.text.SimpleDateFormat SqlTimeFormat = new java.text.SimpleDateFormat("HH:mm:ss.SSS");
	public static java.sql.Time parseSqlTime(String value) {
		try {
			return value == null ? null : new java.sql.Time(SqlTimeFormat.parse(value).getTime());
		} catch (java.text.ParseException e) {
			return null;
		}
	}
	public static String formatSqlTime(java.sql.Time value) {
		return value == null ? null : SqlTimeFormat.format(value);
	}
}
