package com.vasnabilisim.olive.core.paper;

/**
 * Page orientation.
 * 
 * @author Menderes Fatih GUVEN
 */
public enum PaperOrientation {

	Portrait, Landscape;
}
