package com.vasnabilisim.olive.core.paper;

import javax.print.PrintService;

/**
 * Used to represent PrintService.
 * @author Menderes Fatih GUVEN
 */
public class PrintInfo {

	private String name = null;
	private PrintService printservice = null;

	/**
	 * Value constructor.
	 * @param key java.lang.String
	 * @param printservice
	 */
	public PrintInfo(String name, PrintService printservice) {
		this.name = name;
		this.printservice = printservice;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return getName();
	}
	
	/**
	 * Returns the key of the print info.
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the print service.
	 * @return
	 */
	public PrintService getPrintService() {
		return printservice;
	}
}
