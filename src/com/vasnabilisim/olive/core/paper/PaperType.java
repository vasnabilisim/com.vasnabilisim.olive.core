package com.vasnabilisim.olive.core.paper;

import java.io.Serializable;

import com.vasnabilisim.olive.core.geom.Dimension;

/**
 * Page type. 
 * @author Menderes Fatih GUVEN
 */
public class PaperType implements Serializable, Comparable<PaperType> {
	private static final long serialVersionUID = -2065445046695624536L;

	private String name;
	private Dimension size;
	private PaperUnit unit;
	
	public static PaperType Default = new PaperType("A4", 21.0f, 29.7f, PaperUnit.cm);
	
	/**
	 * Default constructor.
	 */
	public PaperType() {
	}

	/**
	 * Value constructor.
	 * @param key
	 * @param width
	 * @param height
	 * @param unit
	 */
	public PaperType(String name, float width, float height, PaperUnit unit) {
		this.name = name;
		this.size = new Dimension(width, height);
		this.unit = unit;
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(PaperType other) {
		if(this == other)
			return 0;
		if(other == null)
			return 1;
		return this.name.compareTo(other.name);
	}
	
	/**
	 * Returns the key of the paper type.
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the width of the paper type.
	 * @return
	 */
	public float getWidth() {
		return size.w;
	}
	
	/**
	 * Returns the height of the paper type.
	 * @return
	 */
	public float getHeight() {
		return size.h;
	}

	/**
	 * Returns the size of the paper type.
	 * @return
	 */
	public Dimension getSize() {
		return size;
	}
	
	/**
	 * Returns the unit of paper type.
	 * @return
	 */
	public PaperUnit getUnit() {
		return unit;
	}
}
