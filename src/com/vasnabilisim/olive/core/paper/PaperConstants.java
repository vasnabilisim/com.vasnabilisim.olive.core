package com.vasnabilisim.olive.core.paper;

/**
 * Print constants.
 * @author Menderes Fatih GUVEN
 */
public interface PaperConstants {

	/**
	 * Cm value of one inch.
	 */
	float INCH_CM	= 2.54f;

	/**
	 * inch value of one cm.
	 */
	float CM_INCH	= 1f / INCH_CM; //0.393701f;
	
	/**
	 * Number of pixels in one inch.
	 */
	float INCH_PIXEL = 72f;
	
	/**
	 * Number of pixels in one cm.
	 */
	float CM_PIXEL = INCH_PIXEL * CM_INCH; //28.346456692913385826771653543307f;
}
