package com.vasnabilisim.olive.core.paper;

/**
 * Page unit
 * 
 * @author Menderes Fatih GUVEN
 */
public enum PaperUnit implements PaperConstants {

	inch, 
	cm
}
