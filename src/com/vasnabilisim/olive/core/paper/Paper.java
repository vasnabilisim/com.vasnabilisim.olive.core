package com.vasnabilisim.olive.core.paper;

import java.io.Serializable;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import com.vasnabilisim.olive.core.geom.Dimension;
import com.vasnabilisim.olive.core.geom.Insets;
import com.vasnabilisim.olive.core.geom.Rectangle;

/**
 * Paper class.
 * 
 * @author Menderes Fatih GUVEN
 */
public class Paper implements java.lang.Cloneable, Serializable {
	private static final long serialVersionUID = 7654174378531825430L;

	private String name;
	private Dimension size = null;
	private Insets margin = null;
	private PaperOrientation orientation = null;
	private PaperUnit unit = null;

	public static final Paper Default = new Paper(
			PaperType.Default, 
			new Insets(1f, 1f, 1f, 1f), 
			PaperOrientation.Portrait, 
			PaperUnit.cm);

	/**
	 * Value constructor.
	 * 
	 * @param paperType
	 */
	public Paper(PaperType paperType) {
		this.name = paperType.getName();
		this.size = paperType.getSize();
		this.margin = new Insets(1f, 1f, 1f, 1f);
		this.orientation = PaperOrientation.Portrait;
		this.unit = paperType.getUnit();
	}

	/**
	 * Value constructor.
	 * 
	 * @param paperType
	 * @param margin
	 * @param orientation
	 */
	public Paper(
			PaperType paperType, 
			Insets margin, 
			PaperOrientation orientation) 
	{
		this.name = paperType.getName();
		this.size = paperType.getSize();
		this.margin = margin;
		this.orientation = orientation;
		this.unit = paperType.getUnit();
	}

	/**
	 * Value constructor.
	 * 
	 * @param paperType
	 * @param margin
	 * @param orientation
	 * @param unit
	 */
	public Paper(
			PaperType paperType, 
			Insets margin, 
			PaperOrientation orientation,
			PaperUnit unit) 
	{
		this.name = paperType.getName();
		this.size = paperType.getSize();
		this.margin = margin;
		this.orientation = orientation;
		this.unit = unit;
	}

	/**
	 * Value constructor.
	 * 
	 * @param size
	 * @param margin
	 * @param orientation
	 * @param unit
	 */
	public Paper(
			Dimension size, 
			Insets margin, 
			PaperOrientation orientation,
			PaperUnit unit) 
	{
		this.name = "Custom";
		this.size = size;
		this.margin = margin;
		this.orientation = orientation;
		this.unit = unit;
	}

	/**
	 * Value constructor.
	 * 
	 * @param name
	 * @param size
	 * @param margin
	 * @param orientation
	 * @param unit
	 */
	public Paper(
			String name, 
			Dimension size, 
			Insets margin, 
			PaperOrientation orientation,
			PaperUnit unit) 
	{
		this.name = name;
		this.size = size;
		this.margin = margin;
		this.orientation = orientation;
		this.unit = unit;
	}

	/**
	 * Copy constructor.
	 * 
	 * @param source
	 */
	public Paper(Paper source) {
		this.name = source.name;
		this.size = source.size.clone();
		this.margin = source.margin.clone();
		this.orientation = source.orientation;
		this.unit = source.unit;
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Paper clone() {
		return new Paper(this);
	}

	/**
	 * Returns the type name. 
	 * Returns "Custom" if no paper type suplied.
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the size.
	 * 
	 * @return
	 */
	public Dimension getSize() {
		return size;
	}

	/**
	 * Returns the width
	 * 
	 * @return
	 */
	public float getWidth() {
		return size.w;
	}

	/**
	 * Returns width if orientation is Portrait, returns height if orientation is Landscape.
	 * 
	 * @return
	 */
	public float getOrientedWidth() {
		return orientation == PaperOrientation.Portrait ? size.w : size.h;
	}

	/**
	 * Returns the width of the imageable rectangle.
	 * 
	 * @return
	 */
	public float getImageableWidth() {
		return size.w - margin.l - margin.r;
	}

	/**
	 * Returns imageable width if orientation is Portrait, returns imageable height if orientation is Landscape.
	 * 
	 * @return
	 */
	public float getOrientedImageableWidth() {
		return orientation == PaperOrientation.Portrait ? getImageableWidth() : getImageableHeight();
	}

	/**
	 * Returns the height
	 * 
	 * @return
	 */
	public float getHeight() {
		return size.h;
	}

	/**
	 * Returns height if orientation is Portrait, returns width if orientation is Landscape.
	 * 
	 * @return
	 */
	public float getOrientedHeight() {
		return orientation == PaperOrientation.Portrait ? size.h : size.w;
	}

	/**
	 * Returns the height of the imageable rectangle.
	 * 
	 * @return
	 */
	public float getImageableHeight() {
		return size.h - margin.t - margin.b;
	}

	/**
	 * Returns imageable height if orientation is Portrait, returns imageable width if orientation is Landscape.
	 * 
	 * @return
	 */
	public float getOrientedImageableHeight() {
		return orientation == PaperOrientation.Portrait ? getImageableHeight() : getImageableWidth();
	}
	
	/**
	 * Returns the margin
	 * @return
	 */
	public Insets getMargin() {
		return margin;
	}

	/**
	 * Returns the left margin.
	 * 
	 * @return
	 */
	public float getLeftMargin() {
		return margin.l;
	}

	/**
	 * Returns left margin if orientation is Portrait, returns top margin if orientation is Landscape.
	 * 
	 * @return
	 */
	public float getOrientedLeftMargin() {
		return orientation == PaperOrientation.Portrait ? margin.l : margin.t;
	}

	/**
	 * Returns the right margin.
	 * 
	 * @return
	 */
	public float getRightMargin() {
		return margin.r;
	}

	/**
	 * Returns right margin if orientation is Portrait, returns bottom margin if orientation is Landscape.
	 * 
	 * @return
	 */
	public float getOrientedRightMargin() {
		return orientation == PaperOrientation.Portrait ? margin.r : margin.b;
	}

	/**
	 * Returns the top margin.
	 * 
	 * @return
	 */
	public float getTopMargin() {
		return margin.t;
	}

	/**
	 * Returns top margin if orientation is Portrait, returns left margin if orientation is Landscape.
	 * 
	 * @return
	 */
	public float getOrientedTopMargin() {
		return orientation == PaperOrientation.Portrait ? margin.t : margin.l;
	}

	/**
	 * Returns the bottom margin.
	 * 
	 * @return
	 */
	public float getBottomMargin() {
		return margin.b;
	}

	/**
	 * Returns bottom margin if orientation is Portrait, returns right margin if orientation is Landscape.
	 * 
	 * @return
	 */
	public float getOrientedBottomMargin() {
		return orientation == PaperOrientation.Portrait ? margin.b : margin.r;
	}

	/**
	 * Returns the imageable bounds.
	 * 
	 * @return
	 */
	public Rectangle getImageableBounds() {
		return new Rectangle(getLeftMargin(), getTopMargin(), getImageableWidth(), getImageableHeight());
	}

	/**
	 * Returns the oriented imageable bounds.
	 * 
	 * @return
	 */
	public Rectangle getOrientedImageableBounds() {
		return new Rectangle(getOrientedLeftMargin(), getOrientedTopMargin(), getOrientedImageableWidth(), getOrientedImageableHeight());
	}

	/**
	 * Returns the orientation.
	 * 
	 * @return
	 */
	public PaperOrientation getOrientation() {
		return orientation;
	}

	/**
	 * Returns the unit.
	 * 
	 * @return
	 */
	public PaperUnit getUnit() {
		return unit;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(100);
		builder.append(name);
		builder.append(",").append(orientation);
		builder.append(",").append(unit);
		builder.append(",").append(size.w);
		builder.append(",").append(size.h);
		builder.append(",").append(margin.l);
		builder.append(",").append(margin.t);
		builder.append(",").append(margin.r);
		builder.append(",").append(margin.b);
		return builder.toString();
	}
	
	public static Paper valueOf(String stringValue) {
		if (stringValue == null)
			return null;
		try {
			StringTokenizer tokenizer = new StringTokenizer(stringValue, ",", false);
			
			String name = tokenizer.nextToken();
			String orientation = tokenizer.nextToken();
			String unit = tokenizer.nextToken();
			String wString = tokenizer.nextToken();
			String hString = tokenizer.nextToken();
			String lString = tokenizer.nextToken();
			String tString = tokenizer.nextToken();
			String rString = tokenizer.nextToken();
			String bString = tokenizer.nextToken();

			PaperOrientation orientationValue = PaperOrientation.valueOf(orientation);
			PaperUnit unitValue = PaperUnit.valueOf(unit);
			
			float w = Float.parseFloat(wString);
			float h = Float.parseFloat(hString);
			float l = Float.parseFloat(lString);
			float t = Float.parseFloat(tString);
			float r = Float.parseFloat(rString);
			float b = Float.parseFloat(bString);

			Paper page = new Paper(
					new Dimension(w, h), 
					new Insets(l, t, r, b), 
					orientationValue, 
					unitValue);
			page.name = name;
			return page;
			
		} catch (	NoSuchElementException | 
					IllegalArgumentException | 
					NullPointerException e) 
		{
			return null;
		}
	}

	/**
	 * Returns an awt paper representing this page.
	 * 
	 * @return
	 */
	public java.awt.print.Paper toPaper() {
		java.awt.print.Paper paper = new java.awt.print.Paper();
		paper.setSize(getWidth(), getHeight());
		paper.setImageableArea(getLeftMargin(), getTopMargin(), getImageableWidth(), getImageableHeight());
		return paper;
	}

	/**
	 * Returns a page format representing this page.
	 * 
	 * @return
	 */
	public java.awt.print.PageFormat toPageFormat() {
		java.awt.print.PageFormat pageFormat = new java.awt.print.PageFormat();
		pageFormat.setPaper(toPaper());
		if (orientation == PaperOrientation.Portrait)
			pageFormat.setOrientation(java.awt.print.PageFormat.PORTRAIT);
		else
			pageFormat.setOrientation(java.awt.print.PageFormat.LANDSCAPE);
		return pageFormat;
	}
}
