package com.vasnabilisim.olive.core.paper;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.vasnabilisim.core.Logger;
import com.vasnabilisim.xml.XmlException;
import com.vasnabilisim.xmlmodel.XmlModel;

/**
 * @author Menderes Fatih GUVEN
 */
public class PaperTypes {

	private List<PaperType> paperTypes = null;
	
	public PaperTypes() {
		paperTypes = new ArrayList<>(30);
	}
	
	public List<PaperType> getPaperTypes() {
		return paperTypes;
	}
	
	public void addPaperType(PaperType paperType) {
		paperTypes.add(paperType);
	}
	
	private static PaperTypes instance;
	public static List<PaperType> get() {
		if(instance == null) {
			try {
				instance = loadPaperTypes();
				Collections.sort(instance.paperTypes);
			} catch (XmlException e) {
				Logger.error(e);
				return new ArrayList<>(0);
			}
		}
		return instance.getPaperTypes();
	}

	public static PaperTypes loadPaperTypes(String fileName) throws XmlException {
		return (PaperTypes) XmlModel.parse(PaperTypes.class.getResourceAsStream("PaperTypes.xmlmodel")).decode(new File(fileName));
	}

	public static PaperTypes loadPaperTypes() throws XmlException {
		return (PaperTypes) XmlModel.parse(PaperTypes.class.getResourceAsStream("PaperTypes.xmlmodel")).decode(PaperTypes.class.getResourceAsStream("PaperTypes.xml"));
	}
}
