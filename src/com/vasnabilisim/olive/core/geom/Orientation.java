package com.vasnabilisim.olive.core.geom;

/**
 * Orientation.
 * 
 * @author Menderes Fatih GUVEN
 */
public enum Orientation {

	Vertical, Horizontal;
}
