package com.vasnabilisim.olive.core.geom;

import java.io.Serializable;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * @author Menderes Fatih GUVEN
 */
public class Point implements Serializable, Cloneable {
	private static final long serialVersionUID = 4047775682925645382L;

	public static final Point Zero = new Point();

    public float x;
    public float y;

    /**
	 * Default constructor.
	 */
	public Point() {
		super();
	}

	/**
	 * Value constructor.
	 * 
	 * @param x
	 * @param y
	 */
	public Point(float x, float y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Copy constructor.
	 * 
	 * @param source
	 */
	public Point(Point source) {
		this.x = source.x;
		this.y = source.y;
	}
	
	/**
	 * @param x
	 * @param y
	 */
	public void setPoint(float x, float y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * @param source
	 */
	public void setPoint(Point source) {
		this.x = source.x;
		this.y = source.y;
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Point clone() {
		return new Point(this);
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(this == obj)
			return true;
		if(!(obj instanceof Point))
			return false;
		Point other = (Point) obj;
		return 	this.x == other.x && this.y == other.y;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(100);
		builder.append(x).append(",").append(y);
		return builder.toString();
	}

	/**
	 * Returns a point instance represented by given string value.
	 * @param stringValue
	 * @return
	 */
	public static Point valueOf(String stringValue) {
		if (stringValue == null) {
			return new Point(0f, 0f);
		}
		try {
			StringTokenizer tokenizer = new StringTokenizer(stringValue, ",", false);
			String x = tokenizer.nextToken();
			String y = tokenizer.nextToken();

			float xValue = java.lang.Float.parseFloat(x);
			float yValue = java.lang.Float.parseFloat(y);

			return new Point(xValue, yValue);
		} catch (NoSuchElementException | NumberFormatException e) {
			return new Point(0f, 0f);
		}
	}
}
