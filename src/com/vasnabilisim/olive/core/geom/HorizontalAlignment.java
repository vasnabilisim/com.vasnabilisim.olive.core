package com.vasnabilisim.olive.core.geom;

/**
 * Horizontal alignment.
 * 
 * @author Menderes Fatih GUVEN
 */
public enum HorizontalAlignment {

	Left, Center, Right, Leading, Trailing;
}
