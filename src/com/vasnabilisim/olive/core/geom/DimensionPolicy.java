package com.vasnabilisim.olive.core.geom;

/**
 * @author Menderes Fatih GUVEN
 */
public enum DimensionPolicy {

	Fill, 
	Fixed,  
	Wrap;
}
