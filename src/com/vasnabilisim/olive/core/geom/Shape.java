package com.vasnabilisim.olive.core.geom;

import java.io.Serializable;

/**
 * @author Menderes Fatih GUVEN
 */
public interface Shape<S extends Shape<S>> extends Cloneable, Serializable {

	/**
	 * Returns the minimum bounds which contains the shape.
	 * @return
	 */
	Rectangle getBounds();
	
	/**
	 * Clones the shape.
	 * @return
	 */
	S clone();
}
