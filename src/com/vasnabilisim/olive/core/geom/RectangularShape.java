package com.vasnabilisim.olive.core.geom;

/**
 * @author Menderes Fatih GUVEN
 */
public abstract class RectangularShape<RS extends RectangularShape<RS>> implements Shape<RS> {
	private static final long serialVersionUID = 6931128242333099929L;
	
	@Override
	public abstract  RS clone();
}
