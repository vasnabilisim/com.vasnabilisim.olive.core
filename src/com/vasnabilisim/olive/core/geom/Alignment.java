package com.vasnabilisim.olive.core.geom;

/**
 * Alignment. 
 * Both a vertical and a horizontal alignment. 
 * 
 * @author Menderes Fatih GUVEN
 */
public enum Alignment {
	
	TopLeft(VerticalAlignment.Top, HorizontalAlignment.Left),
	TopCenter(VerticalAlignment.Top, HorizontalAlignment.Center),
	TopRight(VerticalAlignment.Top, HorizontalAlignment.Right),
	CenterLeft(VerticalAlignment.Center, HorizontalAlignment.Left),
	CenterCenter(VerticalAlignment.Center, HorizontalAlignment.Center),
	CenterRight(VerticalAlignment.Center, HorizontalAlignment.Right),
	BottomLeft(VerticalAlignment.Bottom, HorizontalAlignment.Left),
	BottomCenter(VerticalAlignment.Bottom, HorizontalAlignment.Center),
	BottomRight(VerticalAlignment.Bottom, HorizontalAlignment.Right), 

	TopLeading(VerticalAlignment.Top, HorizontalAlignment.Leading),
	TopTrailing(VerticalAlignment.Top, HorizontalAlignment.Trailing),
	CenterLeading(VerticalAlignment.Center, HorizontalAlignment.Leading),
	CenterTrailing(VerticalAlignment.Center, HorizontalAlignment.Trailing),
	BottomLeading(VerticalAlignment.Bottom, HorizontalAlignment.Leading),
	BottomTrailing(VerticalAlignment.Bottom, HorizontalAlignment.Trailing);
	
	private VerticalAlignment verticalAlignment = VerticalAlignment.Top;
	private HorizontalAlignment horizontalAlignment = HorizontalAlignment.Left;
	
	/**
	 * Constructor.
	 * @param verticalAlignment
	 * @param horizontalAlignment
	 */
	private Alignment(VerticalAlignment verticalAlignment, HorizontalAlignment horizontalAlignment) {
		this.horizontalAlignment = horizontalAlignment;
		this.verticalAlignment = verticalAlignment;
	}
	
	/**
	 * Returns the horizontal alignment.
	 * @return
	 */
	public HorizontalAlignment getHorizontalAlignment() {
		return horizontalAlignment;
	}
	
	/**
	 * Returns the vertical alignment.
	 * @return
	 */
	public VerticalAlignment getVerticalAlignment() {
		return verticalAlignment;
	}
}
