package com.vasnabilisim.olive.core.geom;

import java.io.Serializable;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * @author Menderes Fatih GUVEN
 */
public class Insets implements Serializable, Cloneable {
    private static final long serialVersionUID = -1785440410839161667L;

    public float l;
    public float r;
    public float t;
    public float b;

    public static Insets Zero = new Insets(0f, 0f, 0f, 0f);

	/**
	 * Default constructor.
	 */
	public Insets() {
	}

	/**
	 * Value constructor.
	 * 
	 * @param all
	 */
	public Insets(float all) {
		this.l = all;
		this.t = all;
		this.r = all;
		this.b = all;
	}

	/**
	 * Value constructor.
	 * 
	 * @param l
	 * @param t
	 * @param r
	 * @param b
	 */
	public Insets(float l, float t, float r, float b) {
		this.l = l;
		this.t = t;
		this.r = r;
		this.b = b;
	}

	/**
	 * Copy constructor.
	 * 
	 * @param source
	 */
	public Insets(Insets source) {
		this.l = source.l;
		this.t = source.t;
		this.r = source.r;
		this.b = source.b;
	}
	
	/**
	 * @param l
	 * @param t
	 * @param r
	 * @param b
	 */
	public void setInsets(float l, float t, float r, float b) {
		this.l = l;
		this.t = t;
		this.r = r;
		this.b = b;
	}

	/**
	 * @param source
	 */
	public void setInsets(Insets source) {
		this.l = source.l;
		this.t = source.t;
		this.r = source.r;
		this.b = source.b;
	}
	
	/**
	 * Adds given insets with this. 
	 * Returns a new instance. 
	 * @param insets
	 * @return
	 */
	public Insets merge(Insets insets) {
		Insets result = this.clone();
		result.add(insets);
		return result;
	}
	
	/**
	 * Adds given insets to this.
	 * @param insets
	 */
	public void add(Insets insets) {
		if(insets == null)
			return;
		this.l += insets.l;
		this.t += insets.t;
		this.r += insets.r;
		this.b += insets.b;
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Insets clone() {
		return new Insets(this);
	}

    /**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(this == obj)
			return true;
		if(!(obj instanceof Insets))
			return false;
		Insets other = (Insets) obj;
		return 	this.l == other.l &&
				this.t == other.t && 
				this.r == other.r && 
				this.b == other.b;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(100);
		builder.append(l);
		builder.append(",").append(t);
		builder.append(",").append(r);
		builder.append(",").append(b);
		return builder.toString();
	}

	/**
	 * Returns a rectangle instance represented by given string value.
	 * @param stringValue
	 * @return
	 */
	public static Insets valueOf(String stringValue) {
		if (stringValue == null)
			return Insets.Zero.clone();
		try {
			StringTokenizer tokenizer = new StringTokenizer(stringValue, ",", false);
			String l = tokenizer.nextToken();
			String t = tokenizer.nextToken();
			String r = tokenizer.nextToken();
			String b = tokenizer.nextToken();

			float lValue = java.lang.Float.parseFloat(l);
			float tValue = java.lang.Float.parseFloat(t);
			float rValue = java.lang.Float.parseFloat(r);
			float bValue = java.lang.Float.parseFloat(b);

			return new Insets(lValue, tValue, rValue, bValue);
		} catch (NoSuchElementException | NumberFormatException e) {
			return Insets.Zero.clone();
		}
	}
}
