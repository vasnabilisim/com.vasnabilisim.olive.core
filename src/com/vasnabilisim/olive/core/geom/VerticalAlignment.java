package com.vasnabilisim.olive.core.geom;

/**
 * Vertical alignment.
 * 
 * @author Menderes Fatih GUVEN
 */
public enum VerticalAlignment {

	Top, Center, Bottom;
}
