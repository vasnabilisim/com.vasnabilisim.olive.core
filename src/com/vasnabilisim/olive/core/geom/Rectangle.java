package com.vasnabilisim.olive.core.geom;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * @author Menderes Fatih GUVEN
 */
public class Rectangle extends RectangularShape<Rectangle> {
	private static final long serialVersionUID = -8010652982558701060L;

    public float x;
    public float y;
    public float w;
    public float h;

    public static Rectangle Zero = new Rectangle(0f, 0f, 0f, 0f);

	/**
	 * Default constructor.
	 */
	public Rectangle() {
		super();
	}

	/**
	 * Value constructor.
	 * 
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 */
	public Rectangle(float x, float y, float w, float h) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}

	/**
	 * Copy constructor.
	 * 
	 * @param source
	 */
	public Rectangle(Rectangle source) {
		this.x = source.x;
		this.y = source.y;
		this.w = source.w;
		this.h = source.h;
	}

	/**
	 * @see com.vasnabilisim.olive.core.geom.RectangularShape#clone()
	 */
	@Override
	public Rectangle clone() {
		return new Rectangle(this);
	}
	
	/**
	 * @see com.vasnabilisim.olive.core.geom.Shape#getBounds()
	 */
	@Override
	public Rectangle getBounds() {
		return this;
	}
	
	/**
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 */
	public void setRectangle(float x, float y, float w, float h) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}

	/**
	 * @param source
	 */
	public void setRectangle(Rectangle source) {
		this.x = source.x;
		this.y = source.y;
		this.w = source.w;
		this.h = source.h;
	}
	
	/**
	 * Returns x coordinate of the rectangle.
	 * @return
	 */
	public float getX() {
		return this.x;
	}
	
	/**
	 * Returns x coordinate of the the left edge of the rectangle
	 * @return
	 */
	public float getX1() {
		return x;
	}
	
	/**
	 * Returns x coordinate of the the right edge of the rectangle
	 * @return
	 */
	public float getX2() {
		return x + w;
	}
	
	/**
	 * Sets x coordinate of the rectangle.
	 * @param x
	 */
	public void setX(float x) {
		this.x = x;
	}

	/**
	 * Returns y coordinate of the rectangle.
	 * @return
	 */
	public float getY() {
		return this.y;
	}
	
	/**
	 * Returns the y coordinate of the top edge of the rectangle
	 * @return
	 */
	public float getY1() {
		return y;
	}
	
	/**
	 * Returns the y coordinate of the bottom edge of the rectangle
	 * @return
	 */
	public float getY2() {
		return y;
	}
	
	/**
	 * Sets y coordinate of the rectangle.
	 * @param y
	 */
	public void setY(float y) {
		this.y = y;
	}

	/**
	 * Returns width of the rectangle.
	 * @return
	 */
	public float getWidth() {
		return this.w;
	}
	
	/**
	 * Sets width of the rectangle.
	 * @param y
	 */
	public void setWidth(float w) {
		this.w = w;
	}

	/**
	 * Returns height of the rectangle.
	 * @return
	 */
	public float getHeight() {
		return this.h;
	}
	
	/**
	 * Sets height of the rectangle.
	 * @param y
	 */
	public void setHeight(float h) {
		this.h = h;
	}

	/**
	 * Returns x y coordinates of the rectangle.
	 * @return
	 */
	public Point getPoint() {
		return new Point(x, y);
	}

	/**
	 * Sets x y coordinates of the rectangle.
	 * @param location
	 */
	public void setPoint(Point location) {
		this.x = location.x;
		this.y = location.y;
	}

	/**
	 * Sets x y coordinates of the rectangle.
	 * @param x
	 * @param y
	 */
	public void setPoint(float x, float y) {
		setRectangle(x, y, w, h);
	}

	/**
	 * Returns width height of the rectangle.
	 * @return
	 */
	public Dimension getDimension() {
		return new Dimension(w, h);
	}
	
	/**
	 * Sets width height of the rectangle.
	 * @param size
	 */
	public void setDimension(Dimension size) {
		setRectangle(x, y, size.w, size.h);
	}
	
	/**
	 * Sets width height of the rectangle.
	 * @param w
	 * @param h
	 */
	public void setDimension(float w, float h) {
		setRectangle(x, y, w, h);
	}
	
    /**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(this == obj)
			return true;
		if(!(obj instanceof Rectangle))
			return false;
		Rectangle other = (Rectangle) obj;
		return 	this.x == other.x &&
				this.y == other.y && 
				this.w == other.w && 
				this.h == other.h;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(100);
		builder.append(x);
		builder.append(",").append(y);
		builder.append(",").append(w);
		builder.append(",").append(h);
		return builder.toString();
	}

	/**
	 * Returns a rectangle instance represented by given string value.
	 * @param stringValue
	 * @return
	 */
	public static Rectangle valueOf(String stringValue) {
		if (stringValue == null) {
			return new Rectangle(0f, 0f, 0f, 0f);
		}
		try {
			StringTokenizer tokenizer = new StringTokenizer(stringValue, ",", false);
			String x = tokenizer.nextToken();
			String y = tokenizer.nextToken();
			String w = tokenizer.nextToken();
			String h = tokenizer.nextToken();

			float xValue = java.lang.Float.parseFloat(x);
			float yValue = java.lang.Float.parseFloat(y);
			float wValue = java.lang.Float.parseFloat(w);
			float hValue = java.lang.Float.parseFloat(h);

			return new Rectangle(xValue, yValue, wValue, hValue);
		} catch (NoSuchElementException | NumberFormatException e) {
			return new Rectangle(0f, 0f, 0f, 0f);
		}
	}
}
