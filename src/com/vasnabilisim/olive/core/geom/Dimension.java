package com.vasnabilisim.olive.core.geom;

import java.io.Serializable;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;


/**
 * @author Menderes Fatih GUVEN
 */
public class Dimension implements Cloneable, Serializable {
	private static final long serialVersionUID = -6433486760531329941L;

	public static final Dimension Zero = new Dimension();
	
	public float w = 0f;
	public float h = 0f;

	/**
	 * Default constructor.
	 */
	public Dimension() {
	}

	/**
	 * Value constructor.
	 * 
	 * @param w
	 * @param h
	 */
	public Dimension(float w, float h) {
		this.w = w;
		this.h = h;
	}

	/**
	 * Copy constructor.
	 * 
	 * @param source
	 */
	public Dimension(Dimension source) {
		this.w = source.w;
		this.h = source.h;
	}

	/**
	 * @param w
	 * @param h
	 */
	public void setDimension(float w, float h) {
		this.w = w;
		this.h = h;
	}

	/**
	 * @param source
	 */
	public void setDimension(Dimension source) {
		this.w = source.w;
		this.h = source.h;
	}

	/**
	 * @param w
	 * @param h
	 */
	public void add(float w, float h) {
		this.w += w;
		this.h += h;
	}

	/**
	 * @param source
	 */
	public void add(Dimension source) {
		this.w += source.w;
		this.h += source.h;
	}

	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Dimension clone() {
		return new Dimension(this);
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(this == obj)
			return true;
		if(!(obj instanceof Dimension))
			return false;
		Dimension other = (Dimension) obj;
		return this.w == other.w && this.h == other.h;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(50);
		builder.append(w).append(",").append(h);
		return builder.toString();
	}
	
	/**
	 * Returns a dimension instance represented by given string value.
	 * @param stringValue
	 * @return
	 */
	public static Dimension valueOf(String stringValue) {
		if (stringValue == null) {
			return new Dimension(100f, 20f);
		}
		try {
			StringTokenizer tokenizer = new StringTokenizer(stringValue, ",", false);
			String w = tokenizer.nextToken();
			String h = tokenizer.nextToken();

			float wValue = java.lang.Float.parseFloat(w);
			float hValue = java.lang.Float.parseFloat(h);

			return new Dimension(wValue, hValue);
		} catch (NoSuchElementException | NumberFormatException e) {
			return new Dimension(100f, 20f);
		}
	}
}
